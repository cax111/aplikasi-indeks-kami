@extends('templates.main')

@section('main-content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="header">
            <h4 class="title"><i class="icon-success ti-info-alt"> </i> Apa itu Aplikasi Indeks KAMI </h4>
            <p class="category"></p>
          </div>
          <div class="content">
            <div class="row">
            @if(Auth::user()->role=='user')
              <div class="col-xs-12">
                <div class="icon-big icon-success text-center">
                  <i class="ti-server"></i>
                </div>
              </div>
              <div class="col-xs-12">
                <div style="text-align:center" class="numbers">
                  <p>
                    Indeks KAMI adalah alat evaluasi untuk menganalisa tingkat kesiapan pengamanan informasi di Instansi pemerintah.
                    Alat evaluasi ini tidak ditujukan untuk menganalisa kelayakan atau efektifitas bentuk pengamanan yang ada,
                    melainkan sebagai perangkat untuk memberikan gambaran kondisi kesiapan (kelengkapan dan kematangan) kerangka kerja keamanan informasi kepada pimpinan Instansi.
                    Evaluasi dilakukan terhadap berbagai area yang menjadi target penerapan keamanan informasi dengan ruang lingkup pembahasan yang juga memenuhi semua aspek keamanan yang didefinisikan oleh standar ISO/IEC 27001:2013.
                  </p>
                  <div style="margin:10px">
                    @if(Session::get('isi_identitas')>0)
                      <a class="btn btn-primary" href="/tampil-asesmen/">Mulai Penilaian</a>
                    @else
                      <a class="btn btn-primary" href="/identitas/">Mulai Penilaian</a>
                    @endif
                  </div>
                </div>
              </div>
            @elseif(Auth::user()->role=='assessor')
              <div class="col-xs-12 col-md-12">
                <div class="text-center">Statistik Pengguna</div>
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                  <canvas id="chart1" height="70%"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "2014",
                            "2015",
                            "2016",
                            "2017",
                            "2018",
                            "2019",
                            "2020",
                            "2021"],
                          datasets:[
                          {
                            label: "Asesmen Pengguna / Tahun",
                            data: [65,59,80,81,56,55,40],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart1').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                  <div class="col-xs-12 col-md-6">
                  <canvas id="chart2"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "Minggu Ke-1",
                            "Minggu Ke-2",
                            "Minggu Ke-3",
                            "Minggu Ke-4"],
                          datasets:[
                          {
                            label: "Asesmen Bulan Ini",
                            data: [65,59,80,81],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart2').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                  <div class="col-xs-12 col-md-6">
                  <canvas id="chart3"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "Januari",
                            "Februari",
                            "Maret",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "Agustus",
                            "September",
                            "Oktober",
                            "November",
                            "Desember"],
                          datasets:[
                          {
                            label: "Asesmen Pengguna / Bulan",
                            data: [65,59,80,81,56,55,40,67,77,84,80,81,56],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart3').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                </div>
              </div>
            @else
              <div class="col-xs-12 col-md-12">
                <div class="text-center">Statistik Pengguna</div>
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                  <canvas id="chart1" height="70%"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "2014",
                            "2015",
                            "2016",
                            "2017",
                            "2018",
                            "2019",
                            "2020",
                            "2021"],
                          datasets:[
                          {
                            label: "Asesmen Pengguna / Tahun",
                            data: [65,59,80,81,56,55,40],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart1').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                  <div class="col-xs-12 col-md-6">
                  <canvas id="chart2"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "Minggu Ke-1",
                            "Minggu Ke-2",
                            "Minggu Ke-3",
                            "Minggu Ke-4"],
                          datasets:[
                          {
                            label: "Asesmen Bulan Ini",
                            data: [65,59,80,81],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart2').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                  <div class="col-xs-12 col-md-6">
                  <canvas id="chart3"></canvas>
                  <script>
                    var chartdata = {
                        type: 'line',
                        data: {
                          labels:[
                            "Januari",
                            "Februari",
                            "Maret",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "Agustus",
                            "September",
                            "Oktober",
                            "November",
                            "Desember"],
                          datasets:[
                          {
                            label: "Asesmen Pengguna / Bulan",
                            data: [65,59,80,81,56,55,40,67,77,84,80,81,56],
                            fill: false,
                            borderColor:"rgb(75, 192, 192)",
                            lineTension:0.2}]
                          },
                    };
                    var ctx = document.getElementById('chart3').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>
                  </div>
                </div>
              </div>
            @endif
            </div>
            <div class="footer">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
