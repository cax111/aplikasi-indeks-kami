@extends('templates.main')

    @if(Auth::user()->role != "admin")
        <?php redirect()->to('/')->send(); ?>
    @endif

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Ubah Variable</h4>
                </div>
                <div class="content">
                    <form method="POST" action="">
                      {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Variable</label>
                                    <input type="text" name="nama_variable" class="form-control border-input" value="{{ $variable->nama_variable }}" placeholder="Nama variable.." required>
                                    <p class="label label-danger">{{ $errors->first('nama_variable') }}</p>
                                </div>
                            </div>
                        </div>
                        <div style="margin:10px" class="stats">
                            <div class="form-group">
                                <a class="btn btn-warning" href="/tampil-variable">Kembali</a>
                                <button type="submit" class="btn btn-success btn-wd pull-right">Simpan</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
