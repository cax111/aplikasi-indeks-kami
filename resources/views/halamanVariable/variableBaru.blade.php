@extends('templates.main')

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              @if(count($errors)>0)
                {{$errors->first()}}
              @endif
                <div class="header">
                    <h4 class="title"><i class="ti-write"> </i> Daftar Penilaian</h4>
                </div>
                <div class="content">
                  <div class="row">
                    <?php $i=1;?>
                    @foreach($variables as $variable)
                      <div class="col-lg-3 col-sm-6">
                          <div class="card">
                              <div class="content">
                                  <div class="row">
                                      <div class="col-xs-12">
                                          <div class="icon-big icon-primary text-center">
                                              <i class="ti-server"></i>
                                          </div>
                                      </div>
                                      <div class="col-xs-12">
                                          <div style="text-align:center" class="numbers">
                                              <p>{{$variable->nama_variable}}</p>
                                              <div style="margin:10px">
                                                  <a class="btn btn-primary" href="/asesmen/{{$variable->id_variable}}">Mulai Asesmen</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="footer">
                                      <hr />
                                      <div class="stats">
                                            <i class="ti-close icon-danger"></i> Belum diisi
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    @endforeach
                  </div>
                  <div class="footer" style="margin:10px">
                    <p><hr/></p>
                      <div class="stats">
                        <i class="ti-close icon-danger"></i> Belum ditinjau
                      </div>
                      <button class=" pull-right btn btn-success" disabled>Meminta Peninjauan</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
