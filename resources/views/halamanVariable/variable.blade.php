@extends('templates.main')

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              @if(count($errors)>0)
                {{$errors->first()}}
              @endif
                <div class="header">
                    <h4 class="title"><i class="ti-write"> </i> Daftar Penilaian</h4>
                    <p class="category">Ada  Variabel yang disediakan</p>
                @if(count($dataSaran)>0)
                    @foreach($dataSaran as $data)
                    <div class="content">
                    <div id="acordeon">
	    	                <div class="panel-group" id="accordion">
	    	                    <div class="panel panel-border panel-default">
		    	                       <a data-toggle="collapse" href="#collapseOne" class="collapsed" aria-expanded="false">
		    	                          <div class="panel-heading" style="background-color:#54b07d; color:#fff">
		    	                                 <h4 class="panel-title">
		    	                                      Hasil Peninjauan
		    	                                      <i class="ti-angle-down pull-right"></i>
		    	                                 </h4>
		    	                             </div>
		    	                       </a>
		    	                          <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
		    	                               <div class="panel-body">
		    	                                    {!!$data->isi_saran!!}
		    	                               </div>
		    	                          </div>
	    	                    </div>
	    	                </div>
	    	            </div>
                  </div>
                    @endforeach
                @endif
                </div>
                <div class="content">
                  <div class="row">
                    <?php $i=1;?>
                    @foreach($variables as $variable)
                      <div class="col-lg-3 col-sm-6">
                          @php $cek = null ;$pisahData = explode(' ',$sudahDiisi); $panjangData = count($pisahData); @endphp
                          @for($i=0; $i < $panjangData; $i++)
                            @if($variable->id_variable == $pisahData[$i])
                              @php $cek = true; @endphp
                              @break
                            @else
                              @php $cek = false; @endphp
                            @endif
                          @endfor
                          @php $status_review = null; @endphp
                          @foreach($jawaban AS $tes)
                          @php
                            $status_review = $tes->status_review;
                            break;
                          @endphp
                          @endforeach
                          <div class="card">
                              <div class="content">
                                  <div class="row">
                                      <div class="col-xs-12">
                                          <div class="icon-big icon-primary text-center">
                                              <i class="ti-server"></i>
                                          </div>
                                      </div>
                                      <div class="col-xs-12">
                                          <div style="text-align:center" class="numbers">
                                              <p>{{$variable->nama_variable}}</p>
                                              <div style="margin:10px">
                                                @if($status_review==1)
                                                  <a class="btn btn-primary" href="/lihat-asesmen/{{$variable->id_variable.'-'.$id_jawaban}}">Lihat Asesmen</a>
                                                @elseif($cek)
                                                  <a class="btn btn-primary" href="/ubah-asesmen/{{$variable->id_variable.'-'.$id_jawaban}}">Ubah Asesmen</a>
                                                @else
                                                  <a class="btn btn-primary" href="/asesmen/{{$variable->id_variable}}">Mulai Asesmen</a>
                                                @endif
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="footer">
                                      <hr />
                                      <div class="stats">
                                          @if($cek)
                                            <i class="ti-check icon-success"></i> Sudah diisi
                                          @else
                                            <i class="ti-close icon-danger"></i> Belum diisi
                                          @endif
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      @php $status = true; @endphp
                      @if($cek == false && $status == true)
                        @php $status = false; @endphp
                      @endif
                    @endforeach
                  </div>
                  <div class="footer" style="margin:10px">
                    <p><hr/></p>
                      <div class="stats">
                        @if(count($dataSaran)>0)
                              <i class="ti-check icon-success"></i> Sudah ditinjau
                        @else
                          <i class="ti-close icon-danger"></i> Belum ditinjau
                        @endif
                      </div>
                      @foreach($jawaban AS $cek)
                      @if($cek->status_review==0 && $status==true)
                      <a href="/pengajuan-peninjauan/{{$id_jawaban}}" class="pull-right btn btn-success">Meminta Peninjauan</a>
                      @else
                      <button class="pull-right btn btn-success" disabled>Meminta Peninjauan</button>
                      @endif
                      @php break; @endphp
                      @endforeach
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
