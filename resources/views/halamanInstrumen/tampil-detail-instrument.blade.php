@extends('templates.main')

@section('main-content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">
          <h4 class="title"><i class="icon-success ti-info-alt"> </i> Tampil Detail Instrumen </h4>
          @if(count($errors)>0)
            {{$errors->gagalInput}}
          @endif
          <p class="category"><hr/></p>
        </div>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
                <table class="table table-striped">
                      <tr>
                        <th>ID Instrumen</th>
                        <td>{{$instruments[0]->id_instrumen}}</td>
                      </tr>
                      <tr>
                        <th>Nama Variable</th>
                        <td>{{$a[0]->nama_variable}}</td>
                      </tr>
                      <tr>
                        <th>Nama Judul Instrumen</th>
                        <td>{{$a[0]->isi_judul_instrumen}}</td>
                      </tr>
                      <tr>
                        <th>Isi Instrumen</th>
                        <td>{!!$instruments[0]->isi_instrumen!!}</td>
                      </tr>
                      <tr>
                        <th>Bobot Instrumen</th>
                        <td>{{$instruments[0]->bobot_instrumen}}</td>
                      </tr>
                </table>
              </div>
                <div class="col-md-6">
                  <table class="table table-borderless">
                    <tr>
                      <th>Paramater</th>
                    </tr>
                  </table>
                  @foreach($instruments AS $tampil)
                    <ul>
                      <li>{{$tampil->isi_parameter}}</li>
                    </ul>
                  @endforeach
                </div>
            </div>

          </div>
          <div class="footer">
            <hr />
            <div class="stats">
              <div style="margin:10px" class="stats">
                  <a class="btn btn-warning" href="/tampil-instrument/{{$a[0]->id_variable}}">Kembali</a>
              </div>
              <p class="label label-danger">{{ $errors->first('gagal') }}</p>
            </div>
            <div style="margin:10px" class="pull-right">
            <a class="btn btn-warning" href="/ubah-instrument/{{$tampil->id_instrumen}}">Ubah</a>
            <a class="btn btn-danger" href="/hapus-instrument/{{$tampil->id_instrumen}}"  onclick="return confirm('Apakah anda yakin menghapus instrument ini?')">Hapus</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
