@extends('templates.main')

@if(Auth::user()->role != "admin")
  <?php redirect()->to('/')->send(); ?>
@endif
@if($id < 7)
  <?php redirect()->to('/tampil-variable/')->send(); ?>
@endif

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Tambah Instrumen</h4>
                    @if(count($errors)>0)
                      {{$errors}}
                    @endif
                </div>
                <div class="content">
                    <form method="POST" action="">
                      {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Judul Instrumen</label>
                                            <select name="judul_instrument" class="form-control border-input" required>
                                                <option disabled selected value="">Pilih Judul Instrumen..</option>
                                              @foreach($judul_instrument AS $tampil)
                                                @if($instrument[0]->id_judul_instrumen==$tampil->id_judul_instrumen)
                                                <option selected value="{{$tampil->id_judul_instrumen}}">{{$tampil->isi_judul_instrumen}}</option>
                                                @else
                                                <option value="{{$tampil->id_judul_instrumen}}">{{$tampil->isi_judul_instrumen}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                            <p class="label label-danger">{{ $errors->first('username') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bobot Instrumen</label>
                                            <input type="text" name="bobot_instrument" class="form-control border-input" value="{{$instrument[0]->bobot_instrumen}}{{ old('bobot_instrument') }}" placeholder="bobot instrument.." required>
                                            <p class="label label-danger">{{ $errors->first('bobot_instrument') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Isi Instrumen</label>
                                            <textarea name="isi_instrument" class="form-control border-input a" placeholder="Isi Instrumen.." required>{{$instrument[0]->isi_instrumen}}{{ old('isi_instrument') }}</textarea>
                                            <p class="label label-danger">{{ $errors->first('isi_instrument') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                          <label>Isi Parameter/Jawaban</label>
                                          <br/>
                                          @foreach($parameter AS $tampil)
                                            @php $i=0; @endphp
                                            @foreach($detailInstrument AS $detail)
                                              @if($tampil->id_parameter == $detail->id_parameter)
                                              <input type="checkbox" name="parameter[]" value="{{$tampil->id_parameter}}" checked> {{$tampil->isi_parameter}}<br>
                                              @php $i=$detail->id_parameter; @endphp
                                              @endif
                                            @endforeach

                                            @if($tampil->id_parameter != $i)
                                            <input type="checkbox" name="parameter[]" value="{{$tampil->id_parameter}}"> {{$tampil->isi_parameter}}<br>
                                            @endif
                                          @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-warning" href="/tampil-detail-instrument/{{$instrument[0]->id_instrumen}}">Kembali</a>
                                <button type="submit" class="btn btn-success btn-wd pull-right">Simpan</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
