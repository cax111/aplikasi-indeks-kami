@extends('templates.main')

    @if(Auth::user()->role != "admin")
        <?php redirect()->to('/')->send(); ?>
    @endif

@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Tambah Judul Instrumen</h4>
                </div>
                <div class="content">
                    <form method="POST" action="">
                                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Judul Instrumen</label>
                                    <input type="text" name="isi_judul_instrumen" class="form-control border-input" value="{{ old('isi_judul_instrumen') }}" placeholder="Isi judul instrumen.." required>
                                    <p class="label label-danger">{{ $errors->first('isi_judul_instrumen') }}</p>
                                </div>
                            </div>
                        </div>
                        <div style="margin:10px" class="stats">
                            <div class="form-group">
                                <a class="btn btn-warning" href="/tampil-judul-instrument">Kembali</a>
                                <button type="submit" class="btn btn-success btn-wd pull-right">Simpan</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
