@extends('templates.main')

@section('main-content')
    <div class="container-fluid">
    	<div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
        	<div class="card">
        		@if(count($pertanyaan)>0)
          		<div class="header">
              	<h4 class="title">Bagian : {{$pertanyaan[0]['nama_variable']}}</h4>
                <hr/>
                <p class="category">Bagian ini {{$pertanyaan[0]['nama_variable']}}</p>
                <hr/>
                <p class="category">
									<b>[Penilaian]</b>

                  @if(count($errors)>0)
                    {{$errors->first()}}
                  @endif

									@foreach($pertanyaan AS $soal)
										<?php $a = json_decode($soal,true);
										$detail_instrument = $a['detail_instrument'] ?>
										@foreach($detail_instrument AS $jawaban)
											{{$jawaban['isi_parameter']}};
										@endforeach
										<?php break; ?>
									@endforeach
								</p>
                <hr/>
              </div>
							<div class="content table-responsive table-full-width">
								@if(!empty($errors->first('gagal')))
									<div class="alert alert-danger"><?php echo $errors->first('gagal'); ?></div>
								@endif
									<form method="POST" action="">
											{{ csrf_field() }}
					          	<table class="table table-striped">
							        		<thead>
										      		<tr>
											       		<th>No.</th>
												        <th></th>
											       		<th>Isi Instrument</th>
											        	<th>Status</th>
											      	</tr>
									      	</thead>
							        		<tbody>
									        		<?php
									        			$i=1;
									        			$nojudul=0;
									        			$judul='';
									        		?>
															@foreach($pertanyaan AS $soal)
																@if($soal['isi_judul_instrumen']!=$judul)
																	<?php
																		$judul = $soal['isi_judul_instrumen'];
																		$i = 1;
																		$nojudul++;
																	?>
																	<tr>
											          		<td colspan="4" class="category" style="background-color:#333; color:white"># {{$judul}}</td>
																	</tr>
																@endif
																<tr>
																	<td class="text-center">{{$nojudul}}.{{$nojudul}}.{{$i++}}</td>
																	<td class="text-center">{!!$soal->bobot_instrumen!!}</td>
																	<td>{!!$soal->isi_instrumen!!} </td>
																	<td>
																		<?php
																				$a = json_decode($soal,true);
																				$detail_instrument = $a['detail_instrument'];
																		?>
																		<select style="width:250px;" class="form-control border-input" name="status[]" required>
  																		<option class="text-center"disabled value="">Isi Status</option>

                                      @php $temp = null; $temp2 = null; $temp3 = null; @endphp
  																		@foreach($jawabanParameter AS $jawaban)
                                        @if($jawaban['id_instrumen']==$soal['id_instrumen'])
                                          @php
                                            $temp = $jawaban['isi_parameter'];
                                            $temp3 = $jawaban['id_detail_instrumen'];
                                          @endphp
    																		@endif
                                        @php
                                            $temp2 = $jawaban['id_jawaban_instrumen'];
                                        @endphp
                                      @endforeach

																			@foreach($detail_instrument AS $jawaban)
                                        @if($jawaban['isi_parameter']==$temp)
  																			  <option class="text-center" selected value="{{$jawaban['id_detail_instrumen']}}-{{$jawaban['isi_parameter']}}-{{$jawaban['bobot_parameter']}}-{!!$soal->bobot_instrumen!!}-{!!$soal->tingkat_kematangan!!}-{{$temp2}}-{{$temp3}}">{{$jawaban['isi_parameter']}}</option>
                                        @else
                                          <option class="text-center" disabled value="{{$jawaban['id_detail_instrumen']}}-{{$jawaban['isi_parameter']}}-{{$jawaban['bobot_parameter']}}-{!!$soal->bobot_instrumen!!}-{!!$soal->tingkat_kematangan!!}-{{$temp2}}-{{$temp3}}">{{$jawaban['isi_parameter']}}</option>
                                        @endif
                                      @endforeach
																		</select>
																	</td>
																</tr>
															@endforeach
												</tbody>
											</table>
											<div class="col-md12">
												<hr/>
												<div style="margin:10px" class="stats">
													<a class="btn btn-warning" href="/variable/{{$id_jawaban}}">Kembali</a>
												</div>
											</div>
					        </form>
							</div>
  					@else
              <div class="header">
                  <h4 class="title"><i class="icon-danger ti-na"> </i> Oops</h4>
                  <hr/>
              </div>
  	          <div class="content">
  	        		<div class="table">
  		               <h3>Mohon Maaf, Data Belum Tersedia</h3>
  		               <h5>Silakan kembali ke halaman sebelumnya</h5>
  		          </div>
  	          </div>
          	@endif
  	      </div>
  			</div>
  		</div>
    </div>
@stop
