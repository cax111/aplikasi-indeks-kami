@extends('templates.main')

@section('main-content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="header">
            <h4 class="title"><i class="icon-success ti-info-alt"> </i> Tampil Detail Hasil Asesmen </h4>
            <p class="category"><hr/></p>
          </div>
          <div class="content">
            <div class="row">
            @foreach($data AS $tampil)
              @php
                $dataku = $tampil;
              @endphp
            @endforeach
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                          <th>Nama</th>
                          <td>{{$dataku['pengisi_responden']}}</td>
                        </tr>
                        <tr>
                          <th>Satuan Kerja</th>
                          <td>{{$dataku['satuan_kerja']}}</td>
                        </tr>
                        <tr>
                          <th>direktorat</th>
                          <td>{{$dataku['direktorat']}}</td>
                        </tr>
                        <tr>
                          <th>Departemen</th>
                          <td>{{$dataku['departemen']}}</td>
                        </tr>
                        <tr>
                          <th>Alamat 1</th>
                          <td>{{$dataku['alamat1']}}</td>
                        </tr>
                        <tr>
                          <th>Alamat 2</th>
                          <td>{{$dataku['alamat2']}}</td>
                        </tr>
                        <tr>
                          <th>(Kode Area) Nomor Telepon</th>
                          <td>{{$dataku['nomor_telepon']}}</td>
                        </tr>
                        <tr>
                          <th>Email Instansi/Perusahaan</th>
                          <td>{{$dataku['email']}}</td>
                        </tr>
                    </table>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <tr style="background-color:#333; color:white;">
                        <th>Skor SE</th>
                        <td>:</td>
                        <td>
                            @if(empty($skorku[1]))
                              0
                            @else
                              {{$skorku[1]}}
                            @endif
                        </td>
                        <th>Kategori SE</th>
                        <td colspan="3">
                            @if(empty($kategorise))
                              Belum Diisi
                            @else
                              {{$kategorise}}
                            @endif
                        </td>
                      </tr>
                      <tr>
                        <th>Hasil Evaluasi Akhir</th>
                        @if($hasilEvaluasiAkhir=="Tidak Layak")
                        <td colspan="6" class="text-center" style="background-color:#FE0000; color:white;">
                        @elseif($hasilEvaluasiAkhir=="Pemenuhan Kerangka Dasar")
                        <td colspan="6" class="text-center" style="background-color:#FFCC00; color:white;">
                        @elseif($hasilEvaluasiAkhir=="Cukup Baik")
                        <td colspan="6" class="text-center" style="background-color:#C3D79A; color:white;">
                        @elseif($hasilEvaluasiAkhir=="Baik")
                        <td colspan="6" class="text-center" style="background-color:#76923B; color:white;">
                        @else
                        <td colspan="6" class="text-center" style="background-color:#FE0000; color:white;">
                        @endif
                        @if(empty($hasilEvaluasiAkhir))
                          Belum Diisi
                        @else
                        {{$hasilEvaluasiAkhir}}
                        @endif
                        </td>
                      </tr>
                      <tr>
                        <th style="width:40%">Tingkat Kelengkapan Penerapan Standar ISO 27001 sesuai Kategori SE</th>
                        <td colspan="5">
                          <canvas id="SE"></canvas>
                          <script>
                            var bgColor = '#FE0000';
                            if("{{$kategorise}}"=="Rendah"){
                              if({{$skorSemua}}<=174){
                                bgColor = '#FE0000';
                              }else if({{$skorSemua}}<=312){
                                bgColor = '#FFCC00';
                              }else if({{$skorSemua}}<=535){
                                bgColor = '#C3D79A';
                              }else if({{$skorSemua}}<=645){
                                bgColor = '#76923B';
                              }
                            }else if("{{$kategorise}}"=="Tinggi"){
                              if({{$skorSemua}}<=272){
                                bgColor = '#FE0000';
                              }else if({{$skorSemua}}<=455){
                                bgColor = '#FFCC00';
                              }else if({{$skorSemua}}<=583){
                                bgColor = '#C3D79A';
                              }else if({{$skorSemua}}<=645){
                                bgColor = '#76923B';
                              }
                            }else if("{{$kategorise}}"=="Strategis"){
                              if({{$skorSemua}}<=333){
                                bgColor = '#FE0000';
                              }else if({{$skorSemua}}<=535){
                                bgColor = '#FFCC00';
                              }else if({{$skorSemua}}<=609){
                                bgColor = '#C3D79A';
                              }else if({{$skorSemua}}<=645){
                                bgColor = '#76923B';
                              }
                            }
                            var canvas = document.getElementById('SE');
                            var myLineChart = new Chart(canvas,
                              {
                                  type: 'horizontalBar',
                                  plugins: [{
                                      beforeDraw: function (chart) {
                                          var xaxis = chart.chart.scales["x-axis-0"];
                                          xaxis.ticks = [0,174,312,535];
                                          xaxis._ticks = [{label:0},{label:174},{label:312},{label:535}];
                                          xaxis.ticksAsNumbers = [0,174,312,535];
                                          xaxis.max = 645;
                                          xaxis.end = 645;
                                      }
                                  }],
                                  data: {
                                      labels: [''],
                                      datasets: [
                                          {
                                              label: 'Penerapan Standar ISO 27001',
                                              fill: false,
                                              backgroundColor: bgColor,
                                              data: [{{$skorSemua}}, 645]
                                          }
                                      ]
                                  },
                                  options: {
                                      responsive: true,
                                      title: {
                                          display: true,
                                      },
                                      scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true,
                                              },
                                              ticksAsNumbers: [0,174,312,535]
                                          }]
                                      }
                                  }
                              });
                          </script>
                        </td>
                        <td colspan="1">{{$skorSemua}}</td>
                      </tr>
                      @php $i=0; @endphp
                      @foreach($data2 AS $tampil2)
                      @if($tampil2->id_variable > 1 && $tampil2->id_variable < 7)
                      @php $i++; @endphp
                      <tr>
                          <td>{{$tampil2->nama_variable}}</td>
                          <td>:</td>
                          <td>
                            @if(!empty($skorku[$tampil2->id_variable]))
                              {{$skorku[$tampil2->id_variable]}}
                            @else
                              0
                            @endif
                          </td>
                          <td>Tk Kematangan</td>
                          <td>:</td>
                          <td>
                            @if(!empty($tingkat_Kematangan2[$tampil2->id_variable]))
                              {{$tingkat_Kematangan2[$tampil2->id_variable]}}
                            @else
                              I
                            @endif
                            (
                            @if(!empty($tingkat_Kematangan[$tampil2->id_variable]))
                              {{$tingkat_Kematangan[$tampil2->id_variable]}}
                            @else
                              0
                            @endif
                            )
                          </td>
                          @if($i==1)
                          <td rowspan="5" style="text-align:center">
                          @php $temp = 5; @endphp
                          @if(count($tingkat_Kematangan2)==0)
                            @php $temp = 1; @endphp
                          @endif
                          @foreach($data2 AS $tampil2)
                            @if(!empty($tingkat_Kematangan2[$tampil2->id_variable]))
                              @if($tingkat_Kematangan2[$tampil2->id_variable]=="I")
                                @php
                                $tk = 1;
                                if($temp>$tk){
                                  $temp = $tk;
                                }
                                @endphp
                              @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="I+")
                                @php
                                $tk = 1.5;
                                if($temp>$tk){
                                  $temp = $tk;
                                }
                                @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="II")
                                  @php
                                  $tk = 2;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="II+")
                                  @php
                                  $tk = 2.5;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="III")
                                  @php
                                  $tk = 3;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="III+")
                                  @php
                                  $tk = 3.5;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="IV")
                                  @php
                                  $tk = 4;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="IV+")
                                  @php
                                  $tk = 4.5;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$tampil2->id_variable]=="V")
                                  @php
                                  $tk = 5;
                                  if($temp>$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @endif
                            @endif
                          @endforeach
                          @if($temp==1)
                            I
                          @elseif($temp==1.5)
                            I+
                          @elseif($temp==2)
                            II
                          @elseif($temp==2.5)
                            II+
                          @elseif($temp==3)
                            III
                          @elseif($temp==3.5)
                            III+
                          @elseif($temp==4)
                            IV
                          @elseif($temp==4.5)
                            IV+
                          @elseif($temp==5)
                            V
                          @endif
                            <br/>s/d<br/>
                            @php $temp = 1; @endphp
                            @for($i=0;$i<count($data2);$i++)
                              @if(!empty($tingkat_Kematangan2[$data2[$i]->id_variable]))
                                @if($tingkat_Kematangan2[$data2[$i]->id_variable]=="V")
                                  @php
                                  $tk = 5;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="IV+")
                                  @php
                                  $tk = 4.5;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="IV")
                                  @php
                                  $tk = 4;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="III+")
                                  @php
                                  $tk = 3.5;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="III")
                                  @php
                                  $tk = 3;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="II+")
                                  @php
                                  $tk = 2.5;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="II")
                                  @php
                                  $tk = 2;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="I+")
                                  @php
                                  $tk = 1.5;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp

                                @elseif($tingkat_Kematangan2[$data2[$i]->id_variable]=="I")
                                  @php
                                  $tk = 1;
                                  if($temp<$tk){
                                    $temp = $tk;
                                  }
                                  @endphp
                                @endif
                              @endif
                            @endfor
                            @if($temp==1)
                              I
                            @elseif($temp==1.5)
                              I+
                            @elseif($temp==2)
                              II
                            @elseif($temp==2.5)
                              II+
                            @elseif($temp==3)
                              III
                            @elseif($temp==3.5)
                              III+
                            @elseif($temp==4)
                              IV
                            @elseif($temp==4.5)
                              IV+
                            @elseif($temp==5)
                              V
                            @endif
                          </td>
                          @endif
                      </tr>
                      @endif
                      @endforeach
                      <tr>
                        <td>Pengamanan Keterlibatan Pihak Ketiga</td>
                        <td>:</td>
                        <td colspan="4">{{round(($skorSuplemen1/3)*100)}}%</td>
                        <td style="text-align:center" rowspan="3"> Suplemen </td>
                      </tr>
                      <tr>
                        <td>Pengamanan Layanan Infrastruktur Awan</td>
                        <td>:</td>
                        <td colspan="4">{{round(($skorSuplemen2/3)*100)}}%</td>
                      </tr>
                      <tr>
                        <td>Perlindungan Data Pribadi</td>
                        <td>:</td>
                        <td colspan="4">{{round(($skorSuplemen3/3)*100)}}%</td>
                      </tr>
                      @foreach($data2 AS $tampil2)
                      @if(!empty($tampil2->id_variable))
                      @if($tampil2->id_variable > 7)
                      <tr>
                        <td>{{$tampil2->nama_variable}}</td>
                        <td>:</td>
                        <td colspan="5">
                          @if(!empty($skorku[$tampil2->id_variable]))
                            {{$skorku[$tampil2->id_variable]}}
                          @else
                            0
                          @endif
                        </td>
                      </tr>
                      @endif
                      @endif
                      @endforeach
                    </table>
                  </div>
                </div>
                <div class="col-md-12 text-center" style="padding:5%">
                  <canvas id="canvas"></canvas>
                  <script>
                  var chartdata = {
                  type: 'radar',
                  data: {
                  labels: <?php echo json_encode(array('Tata Kelola', 'Pengelolaan Risiko', 'Kerangka Kerja', 'Pengelolaan Aset', 'Aspek Teknologi')); ?>,
                  // labels: month,
                  datasets: [
                  {
                  label: 'Responden',
                  borderColor: '#E57126',
                  borderColor: '#E57126',
                  borderWidth: 3,
                  data: <?php
                          for($i=2;$i<7;$i++){
                            if(empty($skorku[$i])){
                              $skorku[$i] = 0;
                            }
                          }
                          echo json_encode(array($skorku[2], $skorku[3], $skorku[4], $skorku[5], $skorku[6]));
                        ?>
                  },
                  {
                  label: 'Kerangka Kerja Dasar',
                  backgroundColor: '#ADC684',
                  borderWidth: 1,
                  data: <?php echo json_encode(array(35, 38, 40, 80, 50)); ?>
                  },
                  {
                  label: 'Penerapan Operasional',
                  backgroundColor: '#8EB14D',
                  borderWidth: 1,
                  data: <?php echo json_encode(array(76, 50, 110, 128, 90)); ?>
                  },
                  {
                  label: 'Kepatuhan ISO 27001/SNI',
                  backgroundColor: '#7A943E',
                  borderWidth: 1,
                  data: <?php echo json_encode(array(126, 72, 159, 166, 120)); ?>
                  },
                  ]
                  },
                  options: {
                  scales: {
                  yAxes: [{
                  ticks: {
                  beginAtZero:true
                  }
                  }]
                  }
                  }
                  }
                  var ctx = document.getElementById('canvas').getContext('2d');
                  new Chart(ctx, chartdata);
                  </script>
                </div>
                <div class="col-md-12">
                  <h4>Hasil Prediksi Metode Fuzzy Mamdani</h4>
                  <div class="alert alert-warning">
	                  <span><b> Catatan - </b> Skor hasil prediksi tidak akan keluar jika semua variable belum terisi.</span>
	                </div>
                  Skor Asli : {{$skorSemua}}<br/>
                  Skor Prediksi : {{$defuzifikasi}}

                  <canvas id="chart1" height="70%"></canvas>
                  <script>
                    var chartdata = {
                        type: 'horizontalBar',
                        data: {
                          labels:["Perbandingan Output"],
                          datasets:[
                            {
                              label: "Hasil Asli",
                              data: [{{$skorSemua}}],
                              fill: false,
                              backgroundColor:"#0abde3",
                              lineTension:0.2
                            },
                            {
                              label: "Hasil Prediksi (Fuzzy Mamdani)",
                              data: [{{$defuzifikasi}}],
                              fill: false,
                              backgroundColor:"#ee5253",
                              lineTension:0.2
                            }
                          ],
                        },
                        options:{
                          scales:{
                            xAxes:[{
                                ticks:{
                                  beginAtZero:true
                                }
                              }
                            ]
                          }
                        }
                    };
                    var ctx = document.getElementById('chart1').getContext('2d');
                    new Chart(ctx, chartdata);
                  </script>

                </div>
                <div class="col-md-12">
                  <hr />
                  <div style="margin:10px" class="stats">
                      <a class="btn btn-warning" href="/tampil-hasil-assessment">Kembali</a>
                      @if(count($dataSaran)>0)
                      <a class="pull-right"><button type="button" class="btn btn-success" data-toggle="modal"  data-target="#saranModal">Sudah Ditinjau</button></a>
                      @elseif($dataku->status_review==1)
                      <a class="pull-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Lakukan Peninjauan</button></a>
                      @else
                      <a class="pull-right"><button type="button" class="btn btn-success" disabled>Lakukan Peninjauan</button></a>
                      @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="footer">
              <hr />
              <div class="stats">
                <p class="label label-danger">{{ $errors->first('gagal') }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
<!-- Modal -->
<div class="modal fade" id="saranModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h5 class="modal-title" id="exampleModalLabel">Peninjauan</h5>
        </div>
        <div class="modal-body">
          @foreach($dataSaran as $data)
            {!!$data->isi_saran!!}
            @php break; @endphp
          @endforeach
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
        </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h5 class="modal-title" id="exampleModalLabel">Hasil Peninjauan</h5>
        </div>
        <div class="modal-body">
              {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Isi Peninjauan / Saran</label>
                            <textarea name="isi_saran" class="form-control border-input a" placeholder="Peninjauan / Saran" required>{{ old('isi_saran') }}</textarea>
                            <input type="hidden" name="id_jawaban_instrumen" value="{{$dataku->id_jawaban_instrumen}}">
                            <p class="label label-danger">{{ $errors->first('isi_saran') }}</p>
                        </div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </div>
      </form>
    </div>
  </div>
</div>
