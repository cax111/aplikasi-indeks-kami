@extends('templates.main')

@section('main-content')

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="header">
            <h4 class="title"><i class="icon-success ti-info-alt"> </i> Tampil Hasil Asesmen </h4>
            <p class="category"></p>
          </div>
          <div class="content">
            <div class="row">
              <div class="col-md-12">
                <form method="POST" action="">
                  {{ csrf_field() }}
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Mulai Pengisian</th>
                        <th>Status Pengisian Variable Asesmen</th>
                        <th>Status Peninjauan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                        $j = 0;
                      	if (isset($_GET['page'])) {
                        	$i=(10*$_GET['page'])-9;
                      	}
                      @endphp
                      @if(count($data)==0)
                      <tr>
                        <td colspan="4" class="text-center">Data Instrumen Belum diisi.</td>
                      </tr>
                      @else
                        @foreach($data AS $tampil)
                            <tr>
                              <td>{{$i++}}</td>
                              @php $tanggal = date_create($tampil->created_at); @endphp
                              <td>{!!date_format($tanggal, "d-m-Y")!!}</td>
                              <td>
                                @foreach($data2 AS $tampil2)
                                  @php
                                      $cek = false;
                                  @endphp
                                  @foreach($dataVariableTerisi AS $cekVariable)
                                    @if($tampil->id_user==$cekVariable->id_user && $tampil->id_jawaban_instrumen==$cekVariable->id_jawaban_instrumen)
                                      @if($tampil2->id_variable==$cekVariable->id_variable)
                                        <div class="label label-success">
                                          <i class="icon-default ti-check-box"> </i>{{$tampil2->nama_variable}}
                                        </div>
                                        <br/>
                                        @php $cek = true; @endphp
                                      @endif
                                    @endif
                                  @endforeach
                                      @if(!$cek)
                                        <div class="label label-default">
                                          <i class="icon-default ti-close"> </i>{{$tampil2->nama_variable}}
                                        </div>
                                        <br/>
                                      @endif
                                @endforeach
                              </td>
                              <td>
                              @if(count($dataSaran)==0)
                                <div class="label label-default">
                                  <i class="icon-default ti-close"> </i>Belum ditinjau
                                </div>
                                <br/>
                              @else
                                @foreach($dataSaran AS $datas)
                                    @if($datas->id_jawaban_instrumen==$tampil->id_jawaban_instrumen)
                                      <div class="label label-success">
                                        <i class="icon-default ti-check-box"> </i>Sudah ditinjau
                                      </div>
                                      <br/>
                                    @else
                                      <div class="label label-default">
                                        <i class="icon-default ti-close"> </i>Belum ditinjau
                                      </div>
                                      <br/>
                                    @endif
                                @endforeach
                              @endif
                              </td>
                              <td>
                                <div>
                                  <a class="btn btn-info" href="/variable/{{$tampil->id_jawaban_instrumen}}">Lihat Detail Hasil Asesmen</a>
                                </div>
                              </td>
                            </tr>
                            @php
                              $j = 0;
                            @endphp
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                    <div class="col-md-12">
                      <hr />
                      <div style="margin:10px" class="stats">
                          <a class="btn btn-warning" href="/">Kembali</a>
                            @foreach($data AS $tampil)
                              @if($tampil->status_review==0)
                                <button class="btn btn-success pull-right" disabled>Mulai Asesmen Baru</button>
                                @php break; @endphp
                              @else
                                <a class="btn btn-success pull-right" href="/variable">Mulai Asesmen Baru</a>
                              @endif
                            @endforeach
                      </div>
                    </div>
                </form>
              </div>
            </div>
            <div class="footer">
              <hr />
              <div class="stats">
                <p class="label label-danger">{{ $errors->first('gagal') }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
