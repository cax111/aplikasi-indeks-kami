<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Aplikasi Indeks KAMI</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet"/>

        <!--  Paper Dashboard core CSS    -->
        <link href="{{ URL::asset('css/paper-dashboard.css') }}" rel="stylesheet"/>

        <!--  Fonts and icons     -->
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('css/themify-icons.css') }}" rel="stylesheet">
        <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>

    </head>
    <body>

        <div class="wrapper">
            @include('templates.sidebar')
            @yield('main')
        </div>
    </body>

    <!--   Core JS Files   -->
    <script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>


    <!--  Charts Plugin -->
 <!--    <script src="{{ URL::asset('js/chartist.min.js') }}"></script> -->

    <!--  Notifications Plugin    -->
    <script src="{{ URL::asset('js/bootstrap-notify.js') }}"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="{{ URL::asset('js/paper-dashboard.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
      $('.a').ckeditor();
    </script>
</html>
