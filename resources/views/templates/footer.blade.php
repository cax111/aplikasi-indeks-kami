@extends('main')

@section('footer')
<footer class="footer">
    <div class="container-fluid">
        <div class="copyright pull-right">
            &copy; 2019, This Project made by 
            <a href="https://www.instagram.com/chakrabernatyusuf">Chakra</a>
        </div>
    </div>
</footer>
@stop