
<div class="sidebar" data-background-color="white" data-active-color="primary">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text">
                Aplikasi Indeks KAMI
            </a>
        </div>
        <ul class="nav">
            @if($halaman == "index")
                <li class="active">
                    <a href="/"><i class="ti-panel"></i><p>Home</p></a>
                </li>
            @else
                <li>
                    <a href="/"><i class="ti-panel"></i><p>Home</p></a>
                </li>
            @endif
            @if(Auth::user()->role == "user")
                @if($halaman == "assessment")
                    <li class="active">
                        <a href="/tampil-asesmen"><i class="ti-user"></i><p>Asesmen</p></a>
                    </li>
                @else
                    <li>
                        <a href="/tampil-asesmen"><i class="ti-user"></i><p>Asesmen</p></a>
                    </li>
                @endif
            @endif
            @if(Auth::user()->role == "assessor")
                @if($halaman == "hasilAssessment")
                    <li class="active">
                        <a href="/tampil-hasil-assessment"><i class="ti-pencil-alt2"></i><p>Tampil Hasil Asesmen</p></a>
                    </li>
                @else
                    <li>
                        <a href="/tampil-hasil-assessment"><i class="ti-pencil-alt2"></i><p>Tampil Hasil Assesmen</p></a>
                    </li>
                @endif
            @endif
            @if(Auth::user()->role != "user")
                @if($halaman == "kelolaUser")
                    <li class="active">
                        <a href="/tampil-user"><i class="ti-user"></i><p>Kelola User</p></a>
                    </li>
                @else
                    <li>
                        <a href="/tampil-user"><i class="ti-user"></i><p>Kelola User</p></a>
                    </li>
                @endif
            @endif
            @if(Auth::user()->role == "admin")
                @if($halaman == "kelolaVariable")
                <li class="active">
                    <a href="/tampil-variable"><i class="ti-user"></i><p>Tampil Variable</p></a>
                </li>
                @else
                <li>
                    <a href="/tampil-variable"><i class="ti-user"></i><p>Tampil Variable</p></a>
                </li>
                @endif
            @endif
            @if($halaman == "identitasUser")
                <li class="active">
            @else
                <li>
            @endif
                <a href="/identitas-user">
                    <i class="ti-user"></i>
                    <p>Identitas User</p>
                </a>
                </li>
        </ul>
    </div>
</div>
