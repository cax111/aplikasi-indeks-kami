@extends('templates.template')

@section('main')
<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand" href="/">Indeks KAMI</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="ti-panel"></i>
                            <p>Stats</p>
                        </a>
                    </li> -->
                    <li>
                        <a href="#">
                            <i class="ti-bell"></i>
                            <p class="notification">0</p>
                            <p>Notifikasi</p>
                        </a>
                    </li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
                                <!-- <p class="notification">5</p> -->
                                <p>( {{(Auth::user()->username)}} ) Akun</p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                            <li><a href="#">Ubah Profil</a></li>
                            <li><a href="/logout">Keluar</a></li>
                            </ul>
                    </li>
                    <!-- <li>
                        <a href="#">
                            <i class="ti-settings"></i>
                            <p>Settings</p>
                        </a>
                    </li> -->
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        @yield('main-content')
        @yield('footer')
    </div>
</div>
@stop
