@extends('templates.main')
@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Identitas User Responden</h4><hr/>
                    </div>
                    <div class="content">
                      @if(!empty($identitas))
                        <div class="row">
                            <div class="col-lg-6">
                              <p>Nama User Responden</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->pengisi_responden)) {{$identitas->pengisi_responden}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>NIP</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->nip_responden)) {{$identitas->nip_responden}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Jabatan</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->jabatan_responden)) {{$identitas->jabatan_responden}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Departemen - Direktorat - Satuan Kerja</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->jabatan_responden) || !empty($identitas->jabatan_responden) || !empty($identitas->jabatan_responden)) {{$identitas->departemen}} - {{$identitas->direktorat}} - {{$identitas->satuan_kerja}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Alamat 1</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->alamat1)) {{$identitas->alamat1}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Alamat 2</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->alamat2)) {{$identitas->alamat2}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Kota</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->kota)) {{$identitas->kota}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Kode Pos</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->kode_pos)) {{$identitas->kode_pos}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Nomor Telepon</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->nomor_telepon)) {{$identitas->nomor_telepon}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Email</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->email)) {{$identitas->email}} @else Belum Diisi @endif</p>
                            </div>
                            <div class="col-lg-6">
                              <p>Deskripsi Ruang Lingkup</p>
                            </div>
                            <div class="col-lg-1">
                                <p>:</p>
                            </div>
                            <div class="col-lg-5">
                              <p>@if(!empty($identitas->deskripsi_ruang_lingkup)) {{$identitas->deskripsi_ruang_lingkup}} @else Belum Diisi @endif</p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                          <div class="col-lg-12">
                            <div style="margin:10px" class="stats">
                                <a class="btn btn-warning" href="/">Kembali</a>
                                <a class="btn btn-warning pull-right" href="/ubah-identitas-user">Ubah Profil</a>
                                <a class="btn btn-warning pull-right" href="/ganti-password/{{\Auth::user()->id_user}}">Ubah Password</a>
                            </div>
                          </div>
                        </div>
                        @else

                          <div class="row">
                              <div class="col-lg-6">
                                <p>Nama User Responden</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>NIP</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Jabatan</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Departemen - Direktorat - Satuan Kerja</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Alamat 1</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Alamat 2</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Kota</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Kode Pos</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Nomor Telepon</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Email</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                              <div class="col-lg-6">
                                <p>Deskripsi Ruang Lingkup</p>
                              </div>
                              <div class="col-lg-1">
                                  <p>:</p>
                              </div>
                              <div class="col-lg-5">
                                <p>Belum Diisi</p>
                              </div>
                          </div>
                          <hr/>
                          <div class="row">
                            <div class="col-lg-12">
                              <div style="margin:10px" class="stats">
                                  <a class="btn btn-warning" href="/">Kembali</a>
                                  <a class="btn btn-success pull-right" href="/identitas">Isi Profil</a>
                                  <a class="btn btn-warning pull-right" href="/ganti-password/{{\Auth::user()->id_user}}">Ubah Password</a>
                              </div>
                            </div>
                          </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
