@extends('templates.main')
@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Ubah Password User</h4>
                </div>
                <div class="content">
                    <form method="POST" action="">
                                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password Lama</label>
                                    <input type="password" name="passwordLama" class="form-control border-input" value="{{ old('passwordLama') }}" placeholder="Password Lama.." required>
                                    <p class="label label-danger">{{ $errors->first('passwordLama') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <input type="password" name="passwordBaru" class="form-control border-input" value="{{ old('passwordBaru') }}" placeholder="Password Baru.." required>
                                    <p class="label label-danger">{{ $errors->first('passwordBaru') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Konfirmasi Password Baru</label>
                                    <input type="password" name="konfirmasiPasswordBaru" class="form-control border-input" value="{{ old('konfirmasiPasswordBaru') }}" placeholder="Konfimasi Password Baru.." required>
                                    <p class="label label-danger">{{ $errors->first('konfirmasiPasswordBaru') }}</p>
                                </div>
                            </div>
                        </div>
                        <div style="margin:10px" class="stats">
                            <a class="btn btn-warning" href="/identitas-user">Kembali</a>
                            <button type="submit" class="btn btn-success btn-wd pull-right">Simpan</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
