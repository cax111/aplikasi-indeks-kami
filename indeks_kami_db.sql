-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Agu 2019 pada 09.05
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indeks_kami_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_07_31_153234_create_indentitas_table', 1),
(3, '2019_07_31_155039_create_variable_table', 1),
(4, '2019_07_31_155633_create_parameter_table', 1),
(5, '2019_07_31_160437_create_judul_instrumen_table', 1),
(6, '2019_07_31_160708_create_instrumen_table', 1),
(7, '2019_07_31_160709_create_detail_instrumen_table', 1),
(8, '2019_07_31_160710_create_jawaban_instrumen_table', 1),
(9, '2019_07_31_162832_create_assessment_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_assessment`
--

CREATE TABLE `tb_assessment` (
  `id_assessment` int(11) NOT NULL,
  `id_instrumen` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jawaban_parameter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_instrumen`
--

CREATE TABLE `tb_detail_instrumen` (
  `id_detail_instrumen` int(11) NOT NULL,
  `id_instrumen` int(11) NOT NULL,
  `id_parameter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_detail_instrumen`
--

INSERT INTO `tb_detail_instrumen` (`id_detail_instrumen`, `id_instrumen`, `id_parameter`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 3, 1),
(8, 3, 2),
(9, 3, 3),
(10, 4, 4),
(11, 4, 5),
(12, 4, 6),
(13, 4, 7),
(14, 5, 4),
(15, 5, 5),
(16, 5, 6),
(17, 5, 7),
(18, 6, 4),
(19, 6, 5),
(20, 6, 6),
(21, 6, 7),
(22, 7, 4),
(23, 7, 5),
(24, 7, 6),
(25, 7, 7),
(26, 8, 1),
(27, 8, 2),
(28, 8, 3),
(575, 9, 1),
(576, 9, 2),
(577, 9, 3),
(32, 10, 1),
(33, 10, 2),
(34, 10, 3),
(35, 11, 1),
(36, 11, 2),
(37, 11, 3),
(38, 12, 1),
(39, 12, 2),
(40, 12, 3),
(41, 13, 1),
(42, 13, 2),
(43, 13, 3),
(44, 14, 1),
(45, 14, 2),
(46, 14, 3),
(47, 15, 4),
(48, 15, 5),
(49, 15, 6),
(50, 15, 7),
(51, 16, 4),
(52, 16, 5),
(53, 16, 6),
(54, 16, 7),
(55, 17, 4),
(56, 17, 5),
(57, 17, 6),
(58, 17, 7),
(59, 18, 4),
(60, 18, 5),
(61, 18, 6),
(62, 18, 7),
(63, 19, 4),
(64, 19, 5),
(65, 19, 6),
(66, 19, 7),
(67, 20, 4),
(68, 20, 5),
(69, 20, 6),
(70, 20, 7),
(71, 21, 4),
(72, 21, 5),
(73, 21, 6),
(74, 21, 7),
(75, 22, 4),
(76, 22, 5),
(77, 22, 6),
(78, 22, 7),
(79, 23, 4),
(80, 23, 5),
(81, 23, 6),
(82, 23, 7),
(83, 24, 4),
(84, 24, 5),
(85, 24, 6),
(86, 24, 7),
(87, 25, 4),
(88, 25, 5),
(89, 25, 6),
(90, 25, 7),
(91, 26, 4),
(92, 26, 5),
(93, 26, 6),
(94, 26, 7),
(95, 27, 4),
(96, 27, 5),
(97, 27, 6),
(98, 27, 7),
(99, 28, 4),
(100, 28, 5),
(101, 28, 6),
(102, 28, 7),
(103, 29, 4),
(104, 29, 5),
(105, 29, 6),
(106, 29, 7),
(107, 30, 4),
(108, 30, 5),
(109, 30, 6),
(110, 30, 7),
(111, 31, 4),
(112, 31, 5),
(113, 31, 6),
(114, 31, 7),
(115, 32, 4),
(116, 32, 5),
(117, 32, 6),
(118, 32, 7),
(119, 33, 4),
(120, 33, 5),
(121, 33, 6),
(122, 33, 7),
(123, 34, 4),
(124, 34, 5),
(125, 34, 6),
(126, 34, 7),
(127, 35, 4),
(128, 35, 5),
(129, 35, 6),
(130, 35, 7),
(131, 36, 4),
(132, 36, 5),
(133, 36, 6),
(134, 36, 7),
(135, 37, 4),
(136, 37, 5),
(137, 37, 6),
(138, 37, 7),
(139, 38, 4),
(140, 38, 5),
(141, 38, 6),
(142, 38, 7),
(143, 39, 4),
(144, 39, 5),
(145, 39, 6),
(146, 39, 7),
(147, 40, 4),
(148, 40, 5),
(149, 40, 6),
(150, 40, 7),
(151, 41, 4),
(152, 41, 5),
(153, 41, 6),
(154, 41, 7),
(155, 42, 4),
(156, 42, 5),
(157, 42, 6),
(158, 42, 7),
(159, 43, 4),
(160, 43, 5),
(161, 43, 6),
(162, 43, 7),
(163, 44, 4),
(164, 44, 5),
(165, 44, 6),
(166, 44, 7),
(167, 45, 4),
(168, 45, 5),
(169, 45, 6),
(170, 45, 7),
(171, 46, 4),
(172, 46, 5),
(173, 46, 6),
(174, 46, 7),
(175, 47, 4),
(176, 47, 5),
(177, 47, 6),
(178, 47, 7),
(179, 48, 4),
(180, 48, 5),
(181, 48, 6),
(182, 48, 7),
(183, 49, 4),
(184, 49, 5),
(185, 49, 6),
(186, 49, 7),
(187, 50, 4),
(188, 50, 5),
(189, 50, 6),
(190, 50, 7),
(191, 51, 4),
(192, 51, 5),
(193, 51, 6),
(194, 51, 7),
(195, 52, 4),
(196, 52, 5),
(197, 52, 6),
(198, 52, 7),
(199, 53, 4),
(200, 53, 5),
(201, 53, 6),
(202, 53, 7),
(203, 54, 4),
(204, 54, 5),
(205, 54, 6),
(206, 54, 7),
(207, 55, 4),
(208, 55, 5),
(209, 55, 6),
(210, 55, 7),
(211, 56, 4),
(212, 56, 5),
(213, 56, 6),
(214, 56, 7),
(215, 57, 4),
(216, 57, 5),
(217, 57, 6),
(218, 57, 7),
(219, 58, 4),
(220, 58, 5),
(221, 58, 6),
(222, 58, 7),
(223, 59, 4),
(224, 59, 5),
(225, 59, 6),
(226, 59, 7),
(227, 60, 4),
(228, 60, 5),
(229, 60, 6),
(230, 60, 7),
(231, 61, 4),
(232, 61, 5),
(233, 61, 6),
(234, 61, 7),
(235, 62, 4),
(236, 62, 5),
(237, 62, 6),
(238, 62, 7),
(239, 63, 4),
(240, 63, 5),
(241, 63, 6),
(242, 63, 7),
(243, 64, 4),
(244, 64, 5),
(245, 64, 6),
(246, 64, 7),
(247, 65, 4),
(248, 65, 5),
(249, 65, 6),
(250, 65, 7),
(251, 66, 4),
(252, 66, 5),
(253, 66, 6),
(254, 66, 7),
(255, 67, 4),
(256, 67, 5),
(257, 67, 6),
(258, 67, 7),
(259, 68, 4),
(260, 68, 5),
(261, 68, 6),
(262, 68, 7),
(263, 69, 4),
(264, 69, 5),
(265, 69, 6),
(266, 69, 7),
(267, 70, 4),
(268, 70, 5),
(269, 70, 6),
(270, 70, 7),
(271, 71, 4),
(272, 71, 5),
(273, 71, 6),
(274, 71, 7),
(275, 72, 4),
(276, 72, 5),
(277, 72, 6),
(278, 72, 7),
(279, 73, 4),
(280, 73, 5),
(281, 73, 6),
(282, 73, 7),
(283, 74, 4),
(284, 74, 5),
(285, 74, 6),
(286, 74, 7),
(287, 75, 4),
(288, 75, 5),
(289, 75, 6),
(290, 75, 7),
(291, 76, 4),
(292, 76, 5),
(293, 76, 6),
(294, 76, 7),
(295, 77, 4),
(296, 77, 5),
(297, 77, 6),
(298, 77, 7),
(299, 78, 4),
(300, 78, 5),
(301, 78, 6),
(302, 78, 7),
(303, 79, 4),
(304, 79, 5),
(305, 79, 6),
(306, 79, 7),
(307, 80, 4),
(308, 80, 5),
(309, 80, 6),
(310, 80, 7),
(311, 81, 4),
(312, 81, 5),
(313, 81, 6),
(314, 81, 7),
(315, 82, 4),
(316, 82, 5),
(317, 82, 6),
(318, 82, 7),
(319, 83, 4),
(320, 83, 5),
(321, 83, 6),
(322, 83, 7),
(323, 84, 4),
(324, 84, 5),
(325, 84, 6),
(326, 84, 7),
(327, 85, 4),
(328, 85, 5),
(329, 85, 6),
(330, 85, 7),
(331, 86, 4),
(332, 86, 5),
(333, 86, 6),
(334, 86, 7),
(335, 87, 4),
(336, 87, 5),
(337, 87, 6),
(338, 87, 7),
(339, 88, 4),
(340, 88, 5),
(341, 88, 6),
(342, 88, 7),
(343, 89, 4),
(344, 89, 5),
(345, 89, 6),
(346, 89, 7),
(347, 90, 4),
(348, 90, 5),
(349, 90, 6),
(350, 90, 7),
(351, 91, 4),
(352, 91, 5),
(353, 91, 6),
(354, 91, 7),
(355, 92, 4),
(356, 92, 5),
(357, 92, 6),
(358, 92, 7),
(359, 93, 4),
(360, 93, 5),
(361, 93, 6),
(362, 93, 7),
(363, 94, 4),
(364, 94, 5),
(365, 94, 6),
(366, 94, 7),
(367, 95, 4),
(368, 95, 5),
(369, 95, 6),
(370, 95, 7),
(371, 96, 4),
(372, 96, 5),
(373, 96, 6),
(374, 96, 7),
(375, 97, 4),
(376, 97, 5),
(377, 97, 6),
(378, 97, 7),
(379, 98, 4),
(380, 98, 5),
(381, 98, 6),
(382, 98, 7),
(383, 99, 4),
(384, 99, 5),
(385, 99, 6),
(386, 99, 7),
(387, 100, 4),
(388, 100, 5),
(389, 100, 6),
(390, 100, 7),
(391, 101, 4),
(392, 101, 5),
(393, 101, 6),
(394, 101, 7),
(395, 102, 4),
(396, 102, 5),
(397, 102, 6),
(398, 102, 7),
(399, 103, 4),
(400, 103, 5),
(401, 103, 6),
(402, 103, 7),
(403, 104, 4),
(404, 104, 5),
(405, 104, 6),
(406, 104, 7),
(407, 105, 4),
(408, 105, 5),
(409, 105, 6),
(410, 105, 7),
(411, 106, 4),
(412, 106, 5),
(413, 106, 6),
(414, 106, 7),
(415, 107, 4),
(416, 107, 5),
(417, 107, 6),
(418, 107, 7),
(419, 108, 4),
(420, 108, 5),
(421, 108, 6),
(422, 108, 7),
(423, 109, 4),
(424, 109, 5),
(425, 109, 6),
(426, 109, 7),
(427, 110, 4),
(428, 110, 5),
(429, 110, 6),
(430, 110, 7),
(431, 111, 4),
(432, 111, 5),
(433, 111, 6),
(434, 111, 7),
(435, 112, 4),
(436, 112, 5),
(437, 112, 6),
(438, 112, 7),
(439, 113, 4),
(440, 113, 5),
(441, 113, 6),
(442, 113, 7),
(443, 114, 4),
(444, 114, 5),
(445, 114, 6),
(446, 114, 7),
(447, 115, 4),
(448, 115, 5),
(449, 115, 6),
(450, 115, 7),
(451, 116, 4),
(452, 116, 5),
(453, 116, 6),
(454, 116, 7),
(455, 117, 4),
(456, 117, 5),
(457, 117, 6),
(458, 117, 7),
(459, 118, 4),
(460, 118, 5),
(461, 118, 6),
(462, 118, 7),
(463, 119, 4),
(464, 119, 5),
(465, 119, 6),
(466, 119, 7),
(467, 120, 4),
(468, 120, 5),
(469, 120, 6),
(470, 120, 7),
(471, 121, 4),
(472, 121, 5),
(473, 121, 6),
(474, 121, 7),
(475, 122, 4),
(476, 122, 5),
(477, 122, 6),
(478, 122, 7),
(479, 123, 4),
(480, 123, 5),
(481, 123, 6),
(482, 123, 7),
(483, 124, 4),
(484, 124, 5),
(485, 124, 6),
(486, 124, 7),
(487, 125, 4),
(488, 125, 5),
(489, 125, 6),
(490, 125, 7),
(491, 126, 4),
(492, 126, 5),
(493, 126, 6),
(494, 126, 7),
(495, 127, 4),
(496, 127, 5),
(497, 127, 6),
(498, 127, 7),
(499, 128, 4),
(500, 128, 5),
(501, 128, 6),
(502, 128, 7),
(503, 129, 4),
(504, 129, 5),
(505, 129, 6),
(506, 129, 7),
(507, 130, 4),
(508, 130, 5),
(509, 130, 6),
(510, 130, 7),
(511, 131, 4),
(512, 131, 5),
(513, 131, 6),
(514, 131, 7),
(515, 132, 4),
(516, 132, 5),
(517, 132, 6),
(518, 132, 7),
(519, 133, 4),
(520, 133, 5),
(521, 133, 6),
(522, 133, 7),
(523, 134, 4),
(524, 134, 5),
(525, 134, 6),
(526, 134, 7),
(527, 135, 4),
(528, 135, 5),
(529, 135, 6),
(530, 135, 7),
(531, 136, 4),
(532, 136, 5),
(533, 136, 6),
(534, 136, 7),
(535, 137, 4),
(536, 137, 5),
(537, 137, 6),
(538, 137, 7),
(539, 138, 4),
(540, 138, 5),
(541, 138, 6),
(542, 138, 7),
(543, 139, 4),
(544, 139, 5),
(545, 139, 6),
(546, 139, 7),
(547, 140, 4),
(548, 140, 5),
(549, 140, 6),
(550, 140, 7),
(551, 141, 4),
(552, 141, 5),
(553, 141, 6),
(554, 141, 7),
(578, 147, 4),
(579, 147, 5),
(580, 147, 6),
(581, 147, 7),
(582, 148, 4),
(583, 148, 5),
(584, 148, 6),
(585, 148, 7),
(586, 149, 4),
(587, 149, 5),
(588, 149, 6),
(589, 149, 7),
(590, 150, 4),
(591, 150, 5),
(592, 150, 6),
(593, 150, 7),
(594, 151, 4),
(595, 151, 5),
(596, 151, 6),
(597, 151, 7),
(598, 152, 4),
(599, 152, 5),
(600, 152, 6),
(601, 152, 7),
(602, 153, 4),
(603, 153, 5),
(604, 153, 6),
(605, 153, 7),
(606, 154, 4),
(607, 154, 5),
(608, 154, 6),
(609, 154, 7),
(610, 155, 4),
(611, 155, 5),
(612, 155, 6),
(613, 155, 7),
(614, 156, 4),
(615, 156, 5),
(616, 156, 6),
(617, 156, 7),
(618, 157, 4),
(619, 157, 5),
(620, 157, 6),
(621, 157, 7),
(622, 158, 4),
(623, 158, 5),
(624, 158, 6),
(625, 158, 7),
(626, 159, 4),
(627, 159, 5),
(628, 159, 6),
(629, 159, 7),
(630, 160, 4),
(631, 160, 5),
(632, 160, 6),
(633, 160, 7),
(634, 161, 4),
(635, 161, 5),
(636, 161, 6),
(637, 161, 7),
(638, 162, 4),
(639, 162, 5),
(640, 162, 6),
(641, 162, 7),
(642, 163, 4),
(643, 163, 5),
(644, 163, 6),
(645, 163, 7),
(646, 164, 4),
(647, 164, 5),
(648, 164, 6),
(649, 164, 7),
(650, 165, 4),
(651, 165, 5),
(652, 165, 6),
(653, 165, 7),
(654, 166, 4),
(655, 166, 5),
(656, 166, 6),
(657, 166, 7),
(658, 167, 4),
(659, 167, 5),
(660, 167, 6),
(661, 167, 7),
(662, 168, 4),
(663, 168, 5),
(664, 168, 6),
(665, 168, 7),
(666, 169, 4),
(667, 169, 5),
(668, 169, 6),
(669, 169, 7),
(670, 170, 4),
(671, 170, 5),
(672, 170, 6),
(673, 170, 7),
(674, 171, 4),
(675, 171, 5),
(676, 171, 6),
(677, 171, 7),
(678, 172, 4),
(679, 172, 5),
(680, 172, 6),
(681, 172, 7),
(682, 173, 4),
(683, 173, 5),
(684, 173, 6),
(685, 173, 7),
(686, 174, 4),
(687, 174, 5),
(688, 174, 6),
(689, 174, 7),
(690, 175, 4),
(691, 175, 5),
(692, 175, 6),
(693, 175, 7),
(694, 176, 4),
(695, 176, 5),
(696, 176, 6),
(697, 176, 7),
(698, 177, 4),
(699, 177, 5),
(700, 177, 6),
(701, 177, 7),
(702, 178, 4),
(703, 178, 5),
(704, 178, 6),
(705, 178, 7),
(706, 179, 4),
(707, 179, 5),
(708, 179, 6),
(709, 179, 7),
(710, 180, 4),
(711, 180, 5),
(712, 180, 6),
(713, 180, 7),
(714, 181, 4),
(715, 181, 5),
(716, 181, 6),
(717, 181, 7),
(718, 182, 4),
(719, 182, 5),
(720, 182, 6),
(721, 182, 7),
(722, 183, 4),
(723, 183, 5),
(724, 183, 6),
(725, 183, 7),
(726, 184, 4),
(727, 184, 5),
(728, 184, 6),
(729, 184, 7),
(730, 185, 4),
(731, 185, 5),
(732, 185, 6),
(733, 185, 7),
(734, 186, 4),
(735, 186, 5),
(736, 186, 6),
(737, 186, 7),
(738, 187, 4),
(739, 187, 5),
(740, 187, 6),
(741, 187, 7),
(742, 188, 4),
(743, 188, 5),
(744, 188, 6),
(745, 188, 7),
(746, 189, 4),
(747, 189, 5),
(748, 189, 6),
(749, 189, 7),
(750, 190, 4),
(751, 190, 5),
(752, 190, 6),
(753, 190, 7),
(754, 191, 4),
(755, 191, 5),
(756, 191, 6),
(757, 191, 7),
(766, 194, 4),
(767, 194, 5),
(768, 194, 6),
(769, 194, 7),
(770, 195, 4),
(771, 195, 5),
(772, 195, 6),
(773, 195, 7),
(774, 196, 4),
(775, 196, 5),
(776, 196, 6),
(777, 196, 7),
(778, 197, 4),
(779, 197, 5),
(780, 197, 6),
(781, 197, 7),
(782, 198, 4),
(783, 198, 5),
(784, 198, 6),
(785, 198, 7),
(786, 199, 4),
(787, 199, 5),
(788, 199, 6),
(789, 199, 7),
(790, 200, 4),
(791, 200, 5),
(792, 200, 6),
(793, 200, 7),
(794, 201, 4),
(795, 201, 5),
(796, 201, 6),
(797, 201, 7),
(827, 202, 1),
(828, 202, 2),
(829, 202, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_instrumen`
--

CREATE TABLE `tb_instrumen` (
  `id_instrumen` int(11) NOT NULL,
  `isi_instrumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot_instrumen` int(11) NOT NULL,
  `id_variable` int(11) NOT NULL,
  `id_judul_instrumen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_instrumen`
--

INSERT INTO `tb_instrumen` (`id_instrumen`, `isi_instrumen`, `bobot_instrumen`, `id_variable`, `id_judul_instrumen`) VALUES
(1, '<p>Nilai investasi sistem elektronik yang terpasang<br/>\r\n[A] Lebih dari Rp.30 Miliar<br/>\r\n[B] Lebih dari Rp.3 Miliar s/d Rp.30 Miliar<br/>\r\n[C] Kurang dari Rp.3 Miliar</p>	\r\n', 1, 1, 3),
(2, '<p>Total anggaran operasional tahunan yang dialokasikan untuk pengelolaan Sistem Elektronik<br/>\r\n[A] Lebih dari Rp.10 Miliar<br/>\r\n[B] Lebih dari Rp.1 Miliar s/d Rp.10 Miliar<br/> \r\n[C] Kurang dari Rp.1 Miliar</p>\r\n', 1, 1, 3),
(3, '<p>Memiliki kewajiban kepatuhan terhadap Peraturan atau Standar tertentu<br/>\r\n[A] Peraturan atau Standar nasional dan internasional<br/>\r\n[B] Peraturan atau Standar nasional<br/>\r\n[C] Tidak ada Peraturan khusus</p>\r\n', 1, 1, 3),
(4, '<p>Apakah pimpinan Instansi anda secara prinsip dan resmi bertanggungjawab terhadap pelaksanaan program keamanan informasi (misal yang tercantum dalam ITSP), termasuk penetapan kebijakan terkait?</p>\r\n', 1, 2, 2),
(5, 'Apakah Instansi anda memiliki fungsi atau bagian yang secara spesifik mempunyai tugas dan tanggungjawab mengelola keamanan informasi dan menjaga kepatuhannya? ', 1, 2, 2),
(6, 'Apakah kebijakan dan prosedur maupun dokumen lainnya yang diperlukan terkait keamanan informasi sudah disusun dan dituliskan dengan jelas, dengan mencantumkan peran dan tanggungjawab pihak-pihak yang diberikan wewenang untuk menerapkannya?', 1, 4, 5),
(7, 'Apakah organisasi anda mempunyai strategi penerapan keamanan informasi sesuai hasil analisa risiko yang penerapannya dilakukan sebagai bagian dari rencana kerja organisasi? \r\n', 1, 4, 6),
(8, 'Menggunakan algoritma khusus untuk keamanan informasi dalam Sistem Elektronik <br/>\r\n[A] Algoritma khusus yang digunakan Negara <br/> \r\n[B] Algoritma standar publik <br/>\r\n[C] Tidak ada algoritma khusus', 1, 1, 3),
(9, '<p>Jumlah pengguna Sistem Elektronik<br/>\r\n[A] Lebih dari 5.000 pengguna <br/>\r\n[B] 1.000 sampai dengan 5.000 pengguna <br/>\r\n[C] Kurang dari 1.000 pengguna\r\n</p>', 1, 1, 3),
(10, 'Data pribadi yang dikelola Sistem Elektronik<br/>\r\n[A] Data pribadi yang memiliki hubungan dengan Data Pribadi lainnya <br/>\r\n[B] Data pribadi yang bersifat individu dan/atau data pribadi yang terkait dengan kepemilikan badan usaha<br/>\r\n[C] Tidak ada data pribadi\r\n', 1, 1, 3),
(11, 'Tingkat klasifikasi/kekritisan Data yang ada dalam Sistem Elektronik, relatif terhadap ancaman upaya penyerangan atau penerobosan keamanan informasi<br/>\r\n[A] Sangat Rahasia<br/>\r\n[B] Rahasia dan/ atau Terbatas <br/>\r\n[C] Biasa	\r\n', 1, 1, 3),
(12, 'Tingkat kekritisan proses yang ada dalam Sistem Elektronik, relatif terhadap ancaman upaya penyerangan atau penerobosan keamanan informasi<br/>\r\n[A] Proses yang berisiko mengganggu hajat hidup orang  banyak dan memberi dampak langsung pada layanan publik <br/>\r\n[B] Proses yang berisiko mengganggu hajat hidup orang banyak dan memberi dampak tidak langsung<br/>\r\n[C] Proses yang tidak berdampak bagi kepentingan orang banyak	\r\n', 1, 1, 3),
(13, 'Dampak dari kegagalan Sistem Elektronik<br/>\r\n[A] Tidak tersedianya layanan publik berskala nasional atau membahayakan pertahanan keamanan negara <br/>\r\n[B] Tidak tersedianya layanan publik atau proses penyelenggaraan negara dalam 1 provinsi atau lebih <br/>\r\n[C] Tidak tersedianya layanan publik atau proses penyelenggaraan negara dalam 1 kabupaten/kota atau lebih	\r\n', 1, 1, 3),
(14, 'Potensi kerugian atau dampak negatif dari insiden ditembusnya keamanan informasi Sistem Elektronik (sabotase, terorisme)<br/>\r\n[A] Menimbulkan korban jiwa <br/>\r\n[B] Terbatas pada kerugian finansial <br/>\r\n[C] Mengakibatkan gangguan operasional sementara (tidak membahayakan dan merugikan finansial)\r\n', 1, 1, 3),
(15, 'Apakah pejabat/petugas pelaksana pengamanan informasi mempunyai wewenang yang sesuai untuk menerapkan dan menjamin kepatuhan program keamanan informasi? \r\n', 1, 2, 2),
(16, 'Apakah penanggungjawab pelaksanaan pengamanan informasi diberikan alokasi sumber daya yang sesuai untuk mengelola dan menjamin kepatuhan program keamanan informasi?', 1, 2, 2),
(17, 'Apakah peran pelaksana pengamanan informasi yang mencakup semua keperluan dipetakan dengan lengkap, termasuk kebutuhan audit internal dan persyaratan segregasi kewenangan?', 1, 2, 2),
(18, 'Apakah Instansi anda sudah mendefinisikan persyaratan/standar kompetensi dan keahlian pelaksana pengelolaan keamanan informasi?', 1, 2, 2),
(19, 'Apakah semua pelaksana pengamanan informasi di Instansi anda memiliki kompetensi dan keahlian yang memadai sesuai persyaratan/standar yang berlaku? ', 1, 2, 2),
(20, 'Apakah instansi anda sudah menerapkan program sosialisasi dan peningkatan pemahaman untuk keamanan informasi, termasuk kepentingan kepatuhannya bagi semua pihak yang terkait?', 1, 2, 2),
(21, 'Apakah Instansi anda menerapkan program peningkatan kompetensi dan keahlian untuk pejabat dan petugas pelaksana pengelolaan keamanan informasi? ', 2, 2, 2),
(22, 'Apakah instansi anda sudah mengintegrasikan keperluan/persyaratan keamanan informasi dalam proses kerja yang ada?', 2, 2, 2),
(23, 'Apakah instansi anda sudah mengidentifikasikan data pribadi yang digunakan dalam proses kerja dan menerapkan pengamanan sesuai dengan peraturan perundangan yang berlaku?', 2, 2, 2),
(24, 'Apakah tanggungjawab pengelolaan keamanan informasi mencakup koordinasi dengan pihak pengelola/pengguna aset informasi internal dan eksternal maupun pihak lain yang berkepentingan, untuk mengidentifikasikan persyaratan/kebutuhan pengamanan (misal: pertukaran informasi atau kerjasama yang melibatkan informasi penting) dan menyelesaikan permasalahan yang ada?', 2, 2, 2),
(25, 'Apakah pengelola keamanan informasi secara proaktif berkoordinasi dengan satker terkait (SDM, Legal/Hukum, Umum, Keuangan dll) dan pihak eksternal yang berkepentingan (misal: regulator, aparat keamanan) untuk menerapkan dan menjamin kepatuhan pengamanan informasi terkait proses kerja yang melibatkan berbagai pihak?', 2, 2, 2),
(26, 'Apakah tanggungjawab untuk memutuskan, merancang, melaksanakan dan mengelola langkah kelangsungan layanan TIK (business continuity dan disaster recovery plans) sudah didefinisikan dan dialokasikan?', 2, 2, 2),
(27, 'Apakah penanggungjawab pengelolaan keamanan informasi melaporkan kondisi, kinerja/efektifitas dan kepatuhan program keamanan informasi kepada pimpinan Instansi secara rutin dan resmi?', 2, 2, 2),
(28, 'Apakah kondisi dan permasalahan keamanan informasi di Instansi anda menjadi konsideran atau bagian dari proses pengambilan keputusan strategis di Instansi anda?', 2, 2, 2),
(29, 'Apakah pimpinan satuan kerja di Instansi anda menerapkan program khusus untuk mematuhi tujuan dan sasaran kepatuhan pengamanan informasi, khususnya yang mencakup aset informasi yang menjadi tanggungjawabnya?', 3, 2, 2),
(30, 'Apakah Instansi anda sudah mendefinisikan metrik, paramater dan proses pengukuran kinerja pengelolaan keamanan informasi yang mencakup mekanisme, waktu pengukuran, pelaksananya, pemantauannya dan eskalasi pelaporannya?', 3, 2, 2),
(31, 'Apakah Instansi anda sudah menerapkan program penilaian kinerja pengelolaan keamanan informasi bagi individu (pejabat & petugas) pelaksananya?', 3, 2, 2),
(32, 'Apakah Instansi anda sudah menerapkan target dan sasaran pengelolaan keamanan informasi untuk berbagai area yang relevan, mengevaluasi pencapaiannya secara rutin, menerapkan langkah perbaikan untuk mencapai sasaran yang ada, termasuk pelaporan statusnya kepada pimpinan Instansi?', 3, 2, 2),
(33, 'Apakah Instansi anda sudah mengidentifikasi legislasi, perangkat hukum dan standar lainnya terkait keamanan informasi yang harus dipatuhi dan menganalisa tingkat kepatuhannya?', 3, 2, 2),
(34, 'Apakah Instansi anda sudah mendefinisikan kebijakan dan langkah penanggulangan insiden keamanan informasi yang menyangkut pelanggaran hukum (pidana dan perdata)?', 3, 2, 2),
(35, 'Apakah Instansi anda mempunyai program kerja pengelolaan risiko keamanan informasi yang terdokumentasi dan secara resmi digunakan?', 1, 3, 4),
(36, 'Apakah Instansi anda sudah menetapkan penanggung jawab manajemen risiko dan eskalasi pelaporan status pengelolaan risiko keamanan informasi sampai ke tingkat pimpinan?', 1, 3, 4),
(37, 'Apakah Instansi anda mempunyai kerangka kerja pengelolaan risiko keamanan informasi yang terdokumentasi dan secara resmi digunakan?', 1, 3, 4),
(38, 'Apakah kerangka kerja pengelolaan risiko ini mencakup definisi dan hubungan tingkat klasifikasi aset informasi, tingkat ancaman, kemungkinan terjadinya ancaman tersebut dan dampak kerugian terhadap Instansi anda?', 1, 3, 4),
(39, 'Apakah Instansi anda sudah menetapkan ambang batas tingkat risiko yang dapat diterima?', 1, 3, 4),
(40, 'Apakah Instansi anda sudah mendefinisikan kepemilikan dan pihak pengelola (custodian) aset informasi yang ada, termasuk aset utama/penting dan proses kerja utama yang menggunakan aset tersebut?', 1, 3, 4),
(41, 'Apakah ancaman dan kelemahan yang terkait dengan aset informasi, terutama untuk setiap aset utama sudah teridentifikasi?', 1, 3, 4),
(42, 'Apakah dampak kerugian yang terkait dengan hilangnya/terganggunya fungsi aset utama sudah ditetapkan sesuai dengan definisi yang ada?', 1, 3, 4),
(43, 'Apakah Instansi anda sudah menjalankan inisiatif analisa/kajian risiko keamanan informasi secara terstruktur terhadap aset informasi yang ada (untuk nantinya digunakan dalam mengidentifikasi langkah mitigasi atau penanggulangan yang menjadi bagian dari program pengelolaan keamanan informasi)?', 1, 3, 4),
(44, 'Apakah Instansi anda sudah menyusun langkah mitigasi dan penanggulangan risiko yang ada?', 1, 3, 4),
(45, 'Apakah langkah mitigasi risiko disusun sesuai tingkat prioritas dengan target penyelesaiannya dan penanggungjawabnya, dengan memastikan efektifitas penggunaan sumber daya yang dapat menurunkan tingkat risiko ke ambang batas yang bisa diterima dengan meminimalisir dampak terhadap operasional layanan TIK?', 2, 3, 4),
(46, 'Apakah status penyelesaian langkah mitigasi risiko dipantau secara berkala, untuk memastikan penyelesaian atau kemajuan kerjanya?', 2, 3, 4),
(47, 'Apakah penyelesaian langkah mitigasi yang sudah diterapkan dievaluasi, melalui proses yang obyektif/terukur untuk memastikan konsistensi dan efektifitasnya?', 2, 3, 4),
(48, 'Apakah profil risiko berikut bentuk mitigasinya secara berkala dikaji ulang untuk memastikan akurasi dan validitasnya, termasuk merevisi profil terebut apabila ada perubahan kondisi yang signifikan atau keperluan penerapan bentuk pengamanan baru?', 2, 3, 4),
(49, 'Apakah kerangka kerja pengelolaan risiko secara berkala dikaji untuk memastikan/meningkatkan efektifitasnya?', 3, 3, 4),
(50, 'Apakah pengelolaan risiko menjadi bagian dari kriteria proses penilaian obyektif kinerja efektifitas pengamanan?', 3, 3, 4),
(51, 'Apakah kebijakan keamanan informasi sudah ditetapkan secara formal, dipublikasikan kepada semua staf/karyawan termasuk pihak terkait dan dengan mudah diakses oleh pihak yang membutuhkannya?', 1, 4, 5),
(52, 'Apakah tersedia mekanisme untuk mengelola dokumen kebijakan dan prosedur keamanan informasi, termasuk penggunaan daftar induk, distribusi, penarikan dari peredaran dan penyimpanannya?', 1, 4, 5),
(53, 'Apakah tersedia proses (mencakup pelaksana, mekanisme, jadwal, materi, dan sasarannya) untuk mengkomunikasikan kebijakan keamanan informasi (dan perubahannya) kepada semua pihak terkait, termasuk pihak ketiga?', 1, 4, 5),
(54, 'Apakah keseluruhan kebijakan dan prosedur keamanan informasi yang ada merefleksikan kebutuhan mitigasi dari hasil kajian risiko keamanan informasi, maupun sasaran/obyetif tertentu yang ditetapkan oleh pimpinan Instansi?', 1, 4, 5),
(55, 'Apakah tersedia proses untuk mengidentifikasi kondisi yang membahayakan keamanan infomasi dan menetapkannya sebagai insiden keamanan informasi untuk ditindak lanjuti sesuai prosedur yang diberlakukan?', 1, 4, 5),
(56, 'Apakah aspek keamanan informasi yang mencakup pelaporan insiden, menjaga kerahasiaan, HAKI, tata tertib penggunaan dan pengamanan aset maupun layanan TIK tercantum dalam kontrak dengan pihak ketiga?', 1, 4, 5),
(57, 'Apakah konsekwensi dari pelanggaran kebijakan keamanan informasi sudah didefinisikan, dikomunikasikan dan ditegakkan?', 2, 4, 5),
(58, 'Apakah tersedia prosedur resmi untuk mengelola suatu pengecualian terhadap penerapan keamanan informasi, termasuk proses untuk menindak-lanjuti konsekwensi dari kondisi ini?', 2, 4, 5),
(59, 'Apakah organisasi anda sudah menerapkan kebijakan dan prosedur operasional untuk mengelola implementasi security patch, alokasi tanggungjawab untuk memonitor adanya rilis security patch baru, memastikan pemasangannya dan melaporkannya?', 2, 4, 5),
(60, 'Apakah organisasi anda sudah membahas aspek keamanan informasi dalam manajemen proyek yang terkait dengan ruang lingkup?', 2, 4, 5),
(61, 'Apakah organisasi anda sudah menerapkan proses untuk mengevaluasi risiko terkait rencana pembelian (atau implementasi) sistem baru dan menanggulangi permasalahan yang muncul?', 2, 4, 5),
(62, 'Apakah organisasi anda sudah menerapkan proses pengembangan sistem yang aman (Secure SDLC) dengan menggunakan prinsip atau metode sesuai standar platform teknologi yang digunakan?', 2, 4, 5),
(63, 'Apabila penerapan suatu sistem mengakibatkan timbulnya risiko baru atau terjadinya ketidakpatuhan terhadap kebijakan yang ada, apakah ada proses untuk menanggulangi hal ini, termasuk penerapan pengamanan baru (compensating control) dan jadwal penyelesaiannya?', 2, 4, 5),
(64, 'Apakah tersedia kerangka kerja pengelolaan perencanaan kelangsungan layanan TIK (business continuity planning) yang mendefinisikan persyaratan/konsideran keamanan informasi, termasuk penjadwalan uji-cobanya?', 2, 4, 5),
(65, 'Apakah perencanaan pemulihan bencana terhadap layanan TIK (disaster recovery plan) sudah mendefinisikan komposisi, peran, wewenang dan tanggungjawab tim yang ditunjuk?', 3, 4, 5),
(66, 'Apakah uji-coba perencanaan pemulihan bencana terhadap layanan TIK (disaster recovery plan) sudah dilakukan sesuai jadwal?', 3, 4, 5),
(67, 'Apakah hasil dari perencanaan pemulihan bencana terhadap layanan TIK (disaster recovery plan) dievaluasi untuk menerapkan langkah perbaikan atau pembenahan yang diperlukan (misal, apabila hasil uji-coba menunjukkan bahwa proses pemulihan tidak bisa (gagal) memenuhi persyaratan yang ada?', 3, 4, 5),
(68, 'Apakah seluruh kebijakan dan prosedur keamanan informasi dievaluasi kelayakannya secara berkala?', 3, 4, 5),
(69, 'Apakah organisasi anda mempunyai strategi penggunaan teknologi keamanan informasi yang penerapan dan pemutakhirannya disesuaikan dengan kebutuhan dan perubahan profil risiko?', 1, 4, 6),
(70, 'Apakah strategi penerapan keamanan informasi direalisasikan sebagai bagian dari pelaksanaan program kerja organisasi anda?', 1, 4, 6),
(71, 'Apakah organisasi anda memiliki dan melaksanakan program audit internal yang dilakukan oleh pihak independen dengan cakupan keseluruhan aset informasi, kebijakan dan prosedur keamanan yang ada (atau sesuai dengan standar yang berlaku)?', 1, 4, 6),
(72, 'Apakah audit internal tersebut mengevaluasi tingkat kepatuhan, konsistensi dan efektifitas penerapan keamanan informasi?', 1, 4, 6),
(73, 'Apakah hasil audit internal tersebut dikaji/dievaluasi untuk mengidentifikasi langkah pembenahan dan pencegahan, ataupun inisiatif peningkatan kinerja keamanan informasi?', 2, 4, 6),
(74, 'Apakah hasil audit internal dilaporkan kepada pimpinan organisasi untuk menetapkan langkah perbaikan atau program peningkatan kinerja keamanan informasi?', 2, 4, 6),
(75, 'Apabila ada keperluan untuk merevisi kebijakan dan prosedur yang berlaku, apakah ada analisa untuk menilai  aspek finansial (dampak biaya dan keperluan anggaran) ataupun perubahan terhadap infrastruktur dan pengelolaan perubahannya, sebagai prasyarat untuk menerapkannya? ', 3, 4, 6),
(76, 'Apakah organisasi anda secara periodik menguji dan mengevaluasi tingkat/status kepatuhan program keamanan informasi yang ada (mencakup pengecualian atau kondisi ketidakpatuhan lainnya) untuk memastikan bahwa keseluruhan inisiatif tersebut, termasuk langkah pembenahan yang diperlukan, telah diterapkan secara efektif?', 3, 4, 6),
(77, 'Apakah organisasi anda mempunyai rencana dan program peningkatan keamanan informasi untuk jangka menengah/panjang (1-3-5 tahun) yang direalisasikan secara konsisten?', 3, 4, 6),
(78, 'Apakah tersedia daftar inventaris aset informasi dan aset yang berhubungan dengan proses teknologi informasi secara lengkap, akurat dan terperlihara ? (termasuk kepemilikan aset)', 1, 5, 7),
(79, 'Apakah tersedia definisi klasifikasi aset informasi yang sesuai dengan peraturan perundangan yang berlaku?', 1, 5, 7),
(80, 'Apakah tersedia proses yang mengevaluasi dan mengklasifikasi aset informasi sesuai tingkat kepentingan aset bagi Instansi dan keperluan pengamanannya?', 1, 5, 7),
(81, 'Apakah tersedia definisi tingkatan akses yang berbeda dari setiap klasifikasi aset informasi dan matrix yang merekam alokasi akses tersebut', 1, 5, 7),
(82, 'Apakah tersedia proses pengelolaan perubahan terhadap sistem, proses bisnis dan proses teknologi informasi (termasuk perubahan konfigurasi) yang diterapkan secara konsisten?', 1, 5, 7),
(83, 'Apakah tersedia proses pengelolaan konfigurasi yang diterapkan secara konsisten?', 1, 5, 7),
(84, 'Apakah tersedia proses untuk merilis suatu aset baru ke dalam lingkungan operasional dan memutakhirkan inventaris aset informasi?', 1, 5, 7),
(85, 'Definisi tanggungjawab pengamanan informasi secara individual untuk semua personil di Instansi anda', 1, 5, 7),
(86, 'Tata tertib penggunaan komputer, email, internet dan intranet', 1, 5, 7),
(87, 'Tata tertib pengamanan dan penggunaan aset Instansi terkait HAKI', 1, 5, 7),
(88, 'Peraturan terkait instalasi piranti lunak di aset TI milik instansi', 1, 5, 7),
(89, 'Peraturan penggunaan data pribadi yang mensyaratkan pemberian ijin tertulis oleh pemilik data pribadi', 1, 5, 7),
(90, 'Pengelolaan identitas elektronik dan proses otentikasi (username & password) termasuk kebijakan terhadap pelanggarannya', 1, 5, 7),
(91, 'Persyaratan dan prosedur pengelolaan/pemberian akses, otentikasi dan otorisasi untuk menggunakan aset informasi', 1, 5, 7),
(92, 'Ketetapan terkait waktu penyimpanan untuk klasifikasi data yang ada dan syarat penghancuran data', 1, 5, 7),
(93, 'Ketetapan terkait pertukaran data dengan pihak eksternal dan pengamanannya', 1, 5, 7),
(94, 'Proses penyidikan/investigasi untuk menyelesaikan insiden terkait kegagalan keamanan informasi', 1, 5, 7),
(95, 'Prosedur back-up dan ujicoba pengembalian data (restore) secara berkala ', 1, 5, 7),
(96, 'Ketentuan pengamanan fisik yang disesuaikan dengan definisi zona dan klasifikasi aset yang ada di dalamnya', 2, 5, 7),
(97, 'Proses pengecekan latar belakang SDM', 2, 5, 7),
(98, 'Proses pelaporan insiden keamanan informasi kepada pihak eksternal ataupun pihak yang berwajib.', 2, 5, 7),
(99, 'Prosedur penghancuran data/aset yang sudah tidak diperlukan', 2, 5, 7),
(100, 'Prosedur kajian penggunaan akses (user access review) dan hak aksesnya (user access rights) berikut langkah pembenahan apabila terjadi ketidak sesuaian (non-conformity) terhadap kebijakan yang berlaku', 2, 5, 7),
(101, 'Prosedur untuk user yang mutasi/keluar atau tenaga kontrak/outsource yang habis masa kerjanya.', 2, 5, 7),
(102, 'Apakah tersedia daftar data/informasi yang harus di-backup dan laporan analisa kepatuhan terhadap prosedur backup-nya?', 3, 5, 7),
(103, 'Apakah tersedia daftar rekaman pelaksanaan keamanan informasi dan bentuk pengamanan yang sesuai dengan klasifikasinya?', 3, 5, 7),
(104, 'Apakah tersedia prosedur penggunaan perangkat pengolah informasi milik pihak ketiga (termasuk perangkat milik pribadi dan mitra kerja/vendor) dengan memastikan aspek HAKI dan pengamanan akses yang digunakan?', 3, 5, 7),
(105, 'Apakah sudah diterapkan pengamanan fasilitas fisik (lokasi kerja) yang sesuai dengan kepentingan/klasifikasi aset informasi, secara berlapis dan dapat mencegah upaya akses oleh pihak yang tidak berwenang?	', 1, 5, 8),
(106, 'Apakah tersedia proses untuk mengelola alokasi kunci masuk (fisik dan elektronik) ke fasilitas fisik?', 1, 5, 8),
(107, 'Apakah infrastruktur komputasi terlindungi dari dampak lingkungan atau api dan berada dalam kondisi dengan suhu dan kelembaban yang sesuai dengan prasyarat pabrikannya?', 1, 5, 8),
(108, 'Apakah infrastruktur komputasi yang terpasang terlindungi dari gangguan pasokan listrik atau dampak dari petir?', 1, 5, 8),
(109, 'Apakah tersedia peraturan pengamanan perangkat komputasi milik Instansi anda apabila digunakan di luar lokasi kerja resmi (kantor)?', 1, 5, 8),
(110, 'Apakah tersedia proses untuk memindahkan aset TIK (piranti lunak, perangkat keras, data/informasi dll) dari lokasi yang sudah ditetapkan (dalam daftar inventaris)', 1, 5, 8),
(111, 'Apakah konstruksi ruang penyimpanan perangkat pengolah informasi penting menggunakan rancangan dan material yang dapat menanggulangi risiko kebakaran dan dilengkapi dengan fasilitas pendukung (deteksi kebakaran/asap, pemadam api, pengatur suhu dan kelembaban) yang sesuai?', 2, 5, 8),
(112, 'Apakah tersedia proses untuk memeriksa (inspeksi) dan merawat: perangkat komputer, fasilitas pendukungnya dan kelayakan keamanan lokasi kerja untuk menempatkan aset informasi penting?', 2, 5, 8),
(113, 'Apakah tersedia mekanisme pengamanan dalam pengiriman aset informasi (perangkat dan dokumen) yang melibatkan pihak ketiga?', 2, 5, 8),
(114, 'Apakah tersedia peraturan untuk mengamankan lokasi kerja penting (ruang server, ruang arsip) dari risiko perangkat atau bahan yang dapat membahayakan aset informasi (termasuk fasilitas pengolah informasi) yang ada di dalamnya? (misal larangan penggunaan telpon genggam di dalam ruang server, menggunakan kamera dll)', 2, 5, 8),
(115, 'Apakah tersedia proses untuk mengamankan lokasi kerja dari keberadaan/kehadiran pihak ketiga yang bekerja untuk kepentingan Instansi anda?', 3, 5, 8),
(116, 'Apakah layanan TIK (sistem komputer) yang menggunakan internet sudah dilindungi dengan lebih dari 1 lapis pengamanan?', 1, 6, 1),
(117, 'Apakah jaringan komunikasi disegmentasi sesuai dengan kepentingannya (pembagian Instansi, kebutuhan aplikasi, jalur akses khusus, dll)?', 1, 6, 1),
(118, 'Apakah tersedia konfigurasi standar untuk keamanan sistem bagi keseluruhan aset jaringan, sistem dan aplikasi, yang dimutakhirkan sesuai perkembangan (standar industri yang berlaku) dan kebutuhan?', 1, 6, 1),
(119, 'Apakah Instansi anda secara rutin menganalisa kepatuhan penerapan konfigurasi standar yang ada?', 1, 6, 1),
(120, 'Apakah jaringan, sistem dan aplikasi yang digunakan secara rutin dipindai untuk mengidentifikasi kemungkinan adanya celah kelemahan atau perubahan/keutuhan konfigurasi? ', 1, 6, 1),
(121, 'Apakah keseluruhan infrastruktur jaringan, sistem dan aplikasi dirancang untuk memastikan ketersediaan (rancangan redundan) sesuai kebutuhan/persyaratan yang ada? ', 1, 6, 1),
(122, 'Apakah keseluruhan infrastruktur jaringan, sistem dan aplikasi dimonitor untuk memastikan ketersediaan kapasitas yang cukup untuk kebutuhan yang ada?', 1, 6, 1),
(123, 'Apakah setiap perubahan dalam sistem informasi secara otomatis terekam di dalam log?', 1, 6, 1),
(124, 'PApakah upaya akses oleh yang tidak berhak secara otomatis terekam di dalam log?', 1, 6, 1),
(125, 'Apakah semua log dianalisa secara berkala untuk memastikan akurasi, validitas dan kelengkapan isinya (untuk kepentingan jejak audit dan forensik)?', 1, 6, 1),
(126, 'Apakah Instansi anda menerapkan enkripsi untuk melindungi aset informasi penting sesuai kebijakan pengelolaan yang ada?', 1, 6, 1),
(127, 'Apakah Instansi anda mempunyai standar dalam menggunakan enkripsi?', 2, 6, 1),
(128, 'Apakah Instansi anda menerapkan pengamanan untuk mengelola kunci enkripsi (termasuk sertifikat elektronik) yang digunakan, termasuk siklus penggunaannya?', 2, 6, 1),
(129, 'Apakah semua sistem dan aplikasi secara otomatis mendukung dan menerapkan penggantian password secara otomatis, termasuk menon-aktifkan password, mengatur kompleksitas/panjangnya dan penggunaan kembali password lama?', 2, 6, 1),
(130, 'Apakah akses yang digunakan untuk mengelola sistem (administrasi sistem) menggunakan bentuk pengamanan khusus yang berlapis?', 2, 6, 1),
(131, 'Apakah sistem dan aplikasi yang digunakan sudah menerapkan pembatasan waktu akses termasuk otomatisasi proses timeouts, lockout setelah kegagalan login,dan penarikan akses?', 2, 6, 1),
(132, 'Apakah Instansi anda menerapkan pengamanan untuk mendeteksi dan mencegah penggunaan akses jaringan (termasuk jaringan nirkabel) yang tidak resmi?', 2, 6, 1),
(133, 'Apakah Instansi anda menerapkan bentuk pengamanan khusus untuk melindungi akses dari luar Instansi?', 1, 6, 1),
(134, 'Apakah sistem operasi untuk setiap perangkat desktop dan server dimutakhirkan dengan versi terkini?', 1, 6, 1),
(135, 'Apakah setiap desktop dan server dilindungi dari penyerangan virus (malware)?', 1, 6, 1),
(136, 'Apakah ada rekaman dan hasil analisa (jejak audit - audit trail) yang mengkonfirmasi bahwa antivirus/antimalware telah dimutakhirkan secara rutin dan sistematis?', 2, 6, 1),
(137, 'Apakah adanya laporan penyerangan virus/malware yang gagal/sukses ditindaklanjuti dan diselesaikan?', 2, 6, 1),
(138, 'Apakah keseluruhan jaringan, sistem dan aplikasi sudah menggunakan mekanisme sinkronisasi waktu yang akurat, sesuai dengan standar yang ada?', 2, 6, 1),
(139, 'Apakah setiap aplikasi yang ada memiliki spesifikasi dan fungsi keamanan yang diverifikasi/validasi pada saat proses pengembangan dan uji coba?', 2, 6, 1),
(140, 'Apakah instansi ada menerapkan lingkungan pengembangan dan uji-coba yang sudah diamankan sesuai dengan standar platform teknologi yang ada dan digunakan untuk seluruh siklus hidup sistem yng dibangun?', 3, 6, 1),
(141, 'Apakah Instansi anda melibatkan pihak independen untuk mengkaji kehandalan keamanan informasi secara rutin?', 3, 6, 1),
(147, 'Apakah instansi/perusahaan mengidentifikasi risiko keamanan informasi yang ada terkait dengan kerjasama dengan pihak ketiga atau karyawan kontrak?', 1, 7, 11),
(148, 'Apakah instansi/perusahaan mengkomunikasikan dan mengklarifikasi risiko keamanan informasi yang ada pada pihak ketiga kepada mereka?', 1, 7, 11),
(149, 'Apakah instansi/perusahaan mengklarifikasi persyaratan mitigasi risiko instansi/perusahaan dan ekspektasi mitigasi risiko yang harus dipatuhi oleh pihak ketiga?', 1, 7, 11),
(150, 'Apakah rencana mitigasi terhadap risiko yang diidentifikasi tersebut disetujui oleh manajemen pihak ketiga atau karyawan kontrak?', 1, 7, 11),
(151, 'Apakah instansi/perusahaan telah menerapkan kebijakan keamanan informasi bagi pihak ketiga secara memadai, mencakup persyaratan pengendalian akses, penghancuran informasi,  manajemen risiko penyediaan layanan pihak ketiga, dan NDA bagi karyawan pihak ketiga?', 1, 7, 11),
(152, 'Apakah kebijakan tersebut (7.1.1.5) telah dikomunikasikan kepada pihak ketiga dan mereka menyatakan persetujuannya dalam dokumen kontrak, SLA atau dokumen sejenis lainnya?', 1, 7, 11),
(153, '\"Apakah hak audit TI secara berkala ke pihak ketiga telah ditetapkan sebagai bagian dan persyaratan kontrak, dikomunikasikan dan disetujui pihak ketiga? \r\nTermasuk di dalamnya akses terhadap laporan audit internal/eksternal tentang kondisi kontrol keamanan informasi pihak ketiga?\"', 1, 7, 11),
(154, 'Apakah pihak ketiga sudah mengidentifikasi risiko terkait alih daya, subkontraktor atau penyedia teknologi/infrastruktur yang digunakan dalam layanannya?', 1, 7, 11),
(155, 'Apakah pihak ketiga sudah menerapkan pengendalian risikonya dalam perjanjian dengan mereka atau dokumen sejenis?', 1, 7, 11),
(156, 'Apakah pihak ketiga melakukan pemantauan dan evaluasi terhadap kepatuhan alih daya, subkontraktor atau penyedia teknologi/infrastruktur terhadap persyaratan keamanan yang ditetapkan?', 1, 7, 11),
(157, 'Apakah instansi/perusahaan telah menetapkan proses, prosedur atau rencana terdokumentasi untuk mengelola dan memantau layanan dan aspek keamanan informasi (termasuk pengamanan aset informasi dan infrastruktur milik instanasi/perusahaan yang diakses) dalam hubungan kerjasama dengan pihak ketiga?', 1, 7, 11),
(158, 'Apakah peran dan tanggung jawab pemantauan, evaluasi dan/atau audit aspek keamanan informasi pihak ketiga telah ditetapkan dan/atau ditugaskan dalam unit organisasi tertentu?', 1, 7, 11),
(159, 'Apakah tersedia laporan berkala tentang pencapaian sasaran tingkat layanan (SLA) dan aspek keamanan yang disyaratkan dalam perjanjian komersil (kontrak)?', 1, 7, 11),
(160, 'Apakah ada rapat secara berkala untuk memantau dan mengevaluasi pencapaian sasaran tingkat layanan (SLA) dan aspek keamanan?', 1, 7, 11),
(161, 'Apakah hasil pemantauan dan evaluasi terhadap laporan atau pembahasan dalam rapat berkala tersebut didokumentasikan, dikomunikasikan dan ditindaklanjuti oleh pihak ketiga serta dilaporkan kemajuannya kepada instansi/perusahaan?', 1, 7, 11),
(162, 'Apakah instansi/perusahaan telah menetapkan rencana dan melakukan audit terhadap pemenuhan persyaratan keamanan informasi oleh pihak ketiga?', 1, 7, 11),
(163, 'Apakah hasil audit tersebut ditindaklanjuti oleh pihak ketiga dengan melaporkan rencana perbaikan yang terukur dan bukti-bukti penerapan rencana tersebut?', 1, 7, 11),
(164, 'Apakah kondisi terkait denda/penalti karena ketidakpatuhan pihak ketiga terhadap persyaratan dan/atau tingkat layanan telah didokumentasikan, dikomunikasikan, dipahami dan diterapkan?', 1, 7, 11),
(165, '<p>Apakah instansi/perusahaan mengelola perubahan yang terjadi dalam hubungan dengan pihak ketiga yang menyangkut antara lain?</p>\r\n<ul>\r\n<li>Perubahan layanan pihak ketiga;</li>\r\n<li>Perubahan kebijakan, prosedur, dan/atau</li> \r\n<li>Kontrol risiko pihak ketiga?</li>\r\n</ul>', 1, 7, 11),
(166, 'Apakah risiko yang menyertai perubahan tersebut dikaji, didokumentasikan dan ditetapkan rencana mitigasi barunya?', 1, 7, 11),
(167, 'Apakah pihak ketiga memiliki prosedur formal untuk menangani data selama dalam siklus hidupnya mulai dari pembuatan, pendaftaran, perubahan, dan penghapusan/penghancuran aset?', 1, 7, 11),
(168, 'Apakah per untuk penghancuran (disposal) data secara aman telah disepakati bersama pihak ketiga (pihak ketiga)?', 1, 7, 11),
(169, 'Apakah pihak ketiga memiliki prosedur untuk pelaporan, pemantauan, penanganan, dan analisis insiden keamanan informasi?', 1, 7, 11),
(170, 'Apakah pihak ketiga memiliki bukti-bukti penerapan yang memadai dalam menangani insiden keamanan informasi?', 1, 7, 11),
(171, 'Apakah pihak ketiga memiliki kebijakan, prosedur atau rencana terdokumentasi untuk mengatasi kelangsungan layanan pihak ketiga dalam keadaan darurat/bencana?', 1, 7, 11),
(172, 'Apakah kebijakan, prosedur atau rencana kelangsungan layanan tersebut telah diujicoba, didokumentasikan hasilnya dan dievaluasi efektivitasnya?', 1, 7, 11),
(173, 'Apakah pihak ketiga memiliki organisasi atau tim khusus yang ditugaskan untuk mengelola proses kelangsungan layanannya?', 1, 7, 11),
(174, 'Apakah instansi/perusahaan sudah melakukan kajian risiko terkait penggunaan layanan berbasis cloud dan menyesuaikan kebijakan keamanan informasi terkait layanan ini?', 1, 7, 12),
(175, 'Apakah instansi/perusahaan sudah menetapkan data apa saja yang akan disimpan/diolah/dipertukarkan melalui layanan berbasis cloud?', 1, 7, 12),
(176, 'Apakah instansi/perusahaan sudah menerapkan langkah pengamanan data pribadi yang disimpan/diolah/dipertukarkan melalui layanan cloud?', 1, 7, 12),
(177, 'Apakah instansi/perusahaan sudah mengkaji, menetapkan kriteria dan memastikan aspek hukum (jurisdiksi, hak dan kewenangan) terkait penggunaan layanan berbasis cloud?', 1, 7, 12),
(178, 'Apakah instansi/perusahaan sudah mengevaluasi penyelenggara layanan cloud terkait reputasi penyelenggaranya?', 1, 7, 12),
(179, 'Apakah instansi/perusahaan sudah menetapkan standar keamanan teknis penggunaan layanan cloud, termasuk aspek penggunaannya oleh pengguna di internal instansi/perusahaan?', 1, 7, 12),
(180, 'Apakah instansi/perusahaan sudah mengevaluasi kelaikan keamanan layanan cloud termasuk aspek ketersediaannya dan pemenuhan sertifikasi layanan berbasis ISO 27001?', 1, 7, 12),
(181, 'Apakah instansi/perusahaan sudah memiliki kebijakan, strategi dan proses untuk mengganti layanan cloud atau menyediakan fasilitas pengganti apabila terjadi gangguan sementara pada layanan tersebut?', 1, 7, 12),
(182, 'Apakah instansi/perusahaan sudah memiliki proses pelaporan insiden terkait layanan cloud?', 1, 7, 12),
(183, 'Apakah instansi/perusahaan sudah memiliki proses untuk menghentikan layanan cloud, termasuk proses pengamanan data yang ada (memindahkan dan menghapus data)?', 1, 7, 12),
(184, 'Apakah instansi/perusahaan sudah mendokumentasikan jenis dan bentuk (dokumen kertas/elektronik) data pribadi yang disimpan, diolah dan dipertukarkan dengan pihak eksternal?', 1, 7, 13),
(185, 'Apakah instansi/perusahaan sudah memetakan alur pemrosesan data di internal dan pertukaran data dengan pihak eksternal, termasuk kapan dan dimana data pribadi tersebut diperoleh?', 1, 7, 13),
(186, 'Apakah proses terkait penyimpanan, pengolahan dan pertukaran data pribadi di instansi/perusahaan sudah didokumentasikan?', 1, 7, 13),
(187, 'Apakah instansi/perusahaan sudah memiliki kebijakan terkait Perlindungan Data Pribadi sesuai dengan Peraturan dan Perundangan yang berlaku?', 1, 7, 13),
(188, 'Apakah instansi/perusahaan sudah menunjuk pejabat-pejabat (Data Protection Officer, Data Controller, Data Processor) yang bertanggung-jawab dan berwenang dalam penerapan kebijakan dan proses Perlindungan Data Pribadi?', 1, 7, 13),
(189, 'Apakah instansi/perusahaan sudah menganalisa dampak terkait terungkapnya data pribadi yang disimpan, diolah dan dipertukarkan secara ilegal atau karena insiden lain?', 1, 7, 13),
(190, 'Apakah kajian risiko keamanan pada instansi/perusahaan sudah memasukkan aspek Perlindungan Data Pribadi?', 1, 7, 13),
(191, 'Apakah mekanisme perlindungan data pribadi sudah diterapkan sesuai keperluan mitigasi risiko dan peraturan perundangan yang berlaku?', 1, 7, 13),
(194, 'Apakah instansi/perusahaan sudah menjalankan program peningkatan pemahaman/kepedulian kepada seluruh pegawai terkait Perlindungan Data Pribadi, termasuk hal-hal terkait Peraturan Perundangan yang berlaku?', 1, 7, 13),
(195, 'Apakah instansi/perusahaan sudah mendapatkan persetujuan dari pemilik data pribadi saat mengambil data tersebut, termasuk penjelasan hak pemilik data, apa saja yang akan diberlakukan pada data pribadi tersebut dan menyimpan catatan persetujuan tersebut ?', 1, 7, 13),
(196, 'Apakah instansi/perusahaan sudah memiliki proses untuk melaporkan insiden terkait terungkapnya data pribadi?', 1, 7, 13),
(197, 'Apakah instansi/perusahaan sudah menerapkan proses yang menjamin hak pemilik data pribadi untuk mengakses data tersebut?', 1, 7, 13),
(198, 'Apakah instansi/perusahaan sudah menerapkan proses yang terkait dapat memastikan data pribadi  tersebut akurat dan termutakhirkan?', 1, 7, 13),
(199, 'Apakah instansi/perusahaan sudah menerapkan proses terkait periode penyimpanan data pribadi dan penghapusan/pemusnahannya sesuai dengan peraturan atau perjanjian dengan pemilik data?', 1, 7, 13),
(200, 'Apakah instansi/perusahaan sudah menerapkan proses terkait penghapusan/pemusnahan data apabila sudah tidak ada keperluan yang sah untuk menyimpan/mengolahnya lebih lanjut atau atas permintaan pemilik data dan menyimpan catatan proses tersebut?', 1, 7, 13),
(201, 'Apakah instansi/perusahaan sudah menerapkan proses terkait pengungkapan data pribadi atas permintaan resmi aparat penegak hukum?', 1, 7, 13),
(202, '<p>cek lagi&nbsp;ah....</p>\r\n\r\n<p>eh ko bisa...</p>\r\n\r\n<p>aslina ??</p>', 1, 8, 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jawaban_instrumen`
--

CREATE TABLE `tb_jawaban_instrumen` (
  `id` int(11) NOT NULL,
  `id_jawaban_instrumen` int(11) NOT NULL,
  `skor` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_detail_instrumen` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_jawaban_instrumen`
--

INSERT INTO `tb_jawaban_instrumen` (`id`, `id_jawaban_instrumen`, `skor`, `id_user`, `id_detail_instrumen`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 2, 827, '2019-08-17 06:33:07', '2019-08-17 06:33:07'),
(2, 2, 5, 2, 1, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(3, 2, 1, 2, 6, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(4, 2, 1, 2, 9, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(5, 2, 5, 2, 26, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(6, 2, 2, 2, 576, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(7, 2, 2, 2, 33, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(8, 2, 1, 2, 37, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(9, 2, 5, 2, 38, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(10, 2, 2, 2, 42, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(11, 2, 2, 2, 45, '2019-08-17 10:41:51', '2019-08-17 10:41:51'),
(12, 3, 1, 2, 11, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(13, 3, 2, 2, 16, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(14, 3, 1, 2, 48, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(15, 3, 2, 2, 53, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(16, 3, 3, 2, 58, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(17, 3, 0, 2, 59, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(18, 3, 1, 2, 64, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(19, 3, 3, 2, 70, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(20, 3, 2, 2, 72, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(21, 3, 2, 2, 76, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(22, 3, 6, 2, 82, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(23, 3, 2, 2, 84, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(24, 3, 6, 2, 90, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(25, 3, 6, 2, 94, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(26, 3, 0, 2, 95, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(27, 3, 2, 2, 100, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(28, 3, 6, 2, 105, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(29, 3, 6, 2, 109, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(30, 3, 3, 2, 112, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(31, 3, 9, 2, 118, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(32, 3, 3, 2, 120, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(33, 3, 3, 2, 124, '2019-08-25 02:41:02', '2019-08-25 02:41:02'),
(34, 4, 1, 2, 128, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(35, 4, 1, 2, 132, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(36, 4, 0, 2, 135, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(37, 4, 2, 2, 141, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(38, 4, 3, 2, 146, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(39, 4, 0, 2, 147, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(40, 4, 2, 2, 153, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(41, 4, 1, 2, 156, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(42, 4, 0, 2, 159, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(43, 4, 3, 2, 166, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(44, 4, 0, 2, 167, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(45, 4, 2, 2, 172, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(46, 4, 6, 2, 178, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(47, 4, 0, 2, 179, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(48, 4, 6, 2, 185, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(49, 4, 0, 2, 187, '2019-08-25 02:45:13', '2019-08-25 02:45:13'),
(50, 5, 1, 2, 452, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(51, 5, 1, 2, 456, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(52, 5, 2, 2, 461, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(53, 5, 0, 2, 463, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(54, 5, 1, 2, 468, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(55, 5, 2, 2, 473, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(56, 5, 1, 2, 476, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(57, 5, 0, 2, 479, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(58, 5, 3, 2, 486, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(59, 5, 1, 2, 488, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(60, 5, 1, 2, 492, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(61, 5, 4, 2, 497, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(62, 5, 6, 2, 502, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(63, 5, 0, 2, 503, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(64, 5, 0, 2, 507, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(65, 5, 6, 2, 514, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(66, 5, 2, 2, 516, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(67, 5, 1, 2, 520, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(68, 5, 2, 2, 525, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(69, 5, 1, 2, 528, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(70, 5, 4, 2, 533, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(71, 5, 6, 2, 538, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(72, 5, 0, 2, 539, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(73, 5, 2, 2, 544, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(74, 5, 0, 2, 547, '2019-08-25 02:47:53', '2019-08-25 02:47:53'),
(75, 5, 6, 2, 553, '2019-08-25 02:47:53', '2019-08-25 02:47:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_judul_instrumen`
--

CREATE TABLE `tb_judul_instrumen` (
  `id_judul_instrumen` int(11) NOT NULL,
  `isi_judul_instrumen` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_judul_instrumen`
--

INSERT INTO `tb_judul_instrumen` (`id_judul_instrumen`, `isi_judul_instrumen`) VALUES
(1, 'Pengamanan Teknologi'),
(2, 'Fungsi / Instansi Keamanan Informasi'),
(3, 'Karakteristik Instansi'),
(4, 'Kajian Risiko Keamanan Informasi'),
(5, 'Penyusunan dan Pengelolaan Kebijakan & Prosedur Keamanan Informasi'),
(6, 'Pengelolaan Strategi dan Program Keamanan Informasi'),
(7, 'Pengelolaan Aset Informasi'),
(8, 'Pengamanan Fisik'),
(10, 'Lorem Ipsum'),
(11, 'Pengamanan Keterlibatan Pihak Ketiga Penyedia Layanan'),
(12, 'Pengamanan Layanan Infrastruktur Awan (Cloud Service)'),
(13, 'Perlindungan Data Pribadi'),
(17, 'cek');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_parameter`
--

CREATE TABLE `tb_parameter` (
  `id_parameter` int(11) NOT NULL,
  `isi_parameter` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot_parameter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_parameter`
--

INSERT INTO `tb_parameter` (`id_parameter`, `isi_parameter`, `bobot_parameter`) VALUES
(1, 'A', 5),
(2, 'B', 2),
(3, 'C', 1),
(4, 'Tidak Dilakukan', 0),
(5, 'Dalam Perencanaan', 1),
(6, 'Dalam Penerapan / Diterapkan Sebagian', 2),
(7, 'Diterapkan Secara Menyeluruh', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_responden`
--

CREATE TABLE `tb_responden` (
  `id_responden` int(11) NOT NULL,
  `satuan_kerja` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direktorat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departemen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat1` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat2` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `nomor_telepon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengisi_responden` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip_responden` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_responden` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `deskripsi_ruang_lingkup` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_responden`
--

INSERT INTO `tb_responden` (`id_responden`, `satuan_kerja`, `direktorat`, `departemen`, `alamat1`, `alamat2`, `kota`, `kode_pos`, `nomor_telepon`, `email`, `pengisi_responden`, `nip_responden`, `jabatan_responden`, `tanggal_pengisian`, `deskripsi_ruang_lingkup`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Budaya', 'Direktorat Jenderal Kebudayaan', 'Kementrian Pendidikan dan Kebudayaan', 'Jalan Soekarno - Hatta', 'Jalan Jakarta', 'Bandung', 14045, '(022)5893472', 'jenderalkebudayaan@menteri.com', 'Ahmad Ramdhan', '8957202371923', 'Sekretaris', '2019-05-06 00:00:00', 'Bagus', 2, '2019-05-06 15:51:14', '2019-05-06 15:51:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin123', '$2y$10$g6K0lfZKXLgl8Zej9gqqmenSbosqvNIoxPAuHUOgXdh601cczZkF2', 'admin', '', NULL, NULL),
(2, 'user123', '$2y$10$cpdjAoGMTNyxuQH2s5yvAe7SPD8L.d3kxraoGDD/SQN.wIR.ndwKm', 'user', 'qeb3MZlmKV9jbvYvSTe2aMszpn4hD6lvleXrNybjOiWsd4jmGS9EnYhMkwDC', '2019-08-05 04:00:38', '2019-08-05 04:05:22'),
(3, 'asesor', '$2y$10$6pSbewrUEQ6F3GeFbIG.meleFPPNGI0exA.JTFWVniTEwFEfig9Nq', 'assessor', 'zHqdw9wQX5macuxJuRvOHKAZopoyIFreEdXpWUhoIsxjwp3entHbvcjtXUSI', '2019-08-05 04:01:18', '2019-08-05 04:01:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_variable`
--

CREATE TABLE `tb_variable` (
  `id_variable` int(11) NOT NULL,
  `nama_variable` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_variable`
--

INSERT INTO `tb_variable` (`id_variable`, `nama_variable`) VALUES
(1, 'Kategori Sistem Elektronik'),
(2, 'Tata Kelola Keamanan Informasi'),
(3, 'Pengelolaan Risiko Keamanan Informasi'),
(4, 'Kerangka Kerja Pengelolaan Keamanan Informasi'),
(5, 'Pengelolaan Aset Informasi'),
(6, 'Teknologi dan Keamanan Informasi'),
(7, 'Suplemen'),
(8, 'cobaa');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_assessment`
--
ALTER TABLE `tb_assessment`
  ADD PRIMARY KEY (`id_assessment`),
  ADD KEY `tb_assessment_id_instrumen_id_user_index` (`id_instrumen`,`id_user`),
  ADD KEY `tb_assessment_id_user_foreign` (`id_user`);

--
-- Indeks untuk tabel `tb_detail_instrumen`
--
ALTER TABLE `tb_detail_instrumen`
  ADD PRIMARY KEY (`id_detail_instrumen`),
  ADD KEY `tb_detail_instrumen_id_instrumen_id_parameter_index` (`id_instrumen`,`id_parameter`),
  ADD KEY `tb_detail_instrumen_id_parameter_foreign` (`id_parameter`);

--
-- Indeks untuk tabel `tb_instrumen`
--
ALTER TABLE `tb_instrumen`
  ADD PRIMARY KEY (`id_instrumen`),
  ADD KEY `tb_instrumen_id_variable_id_judul_instrumen_index` (`id_variable`,`id_judul_instrumen`),
  ADD KEY `tb_instrumen_id_judul_instrumen_foreign` (`id_judul_instrumen`);

--
-- Indeks untuk tabel `tb_jawaban_instrumen`
--
ALTER TABLE `tb_jawaban_instrumen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_jawaban_instrumen_id_user_id_detail_instrumen_index` (`id_user`,`id_detail_instrumen`),
  ADD KEY `tb_jawaban_instrumen_id_detail_instrumen_foreign` (`id_detail_instrumen`);

--
-- Indeks untuk tabel `tb_judul_instrumen`
--
ALTER TABLE `tb_judul_instrumen`
  ADD PRIMARY KEY (`id_judul_instrumen`);

--
-- Indeks untuk tabel `tb_parameter`
--
ALTER TABLE `tb_parameter`
  ADD PRIMARY KEY (`id_parameter`);

--
-- Indeks untuk tabel `tb_responden`
--
ALTER TABLE `tb_responden`
  ADD PRIMARY KEY (`id_responden`),
  ADD KEY `tb_responden_id_user_index` (`id_user`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `tb_user_username_unique` (`username`);

--
-- Indeks untuk tabel `tb_variable`
--
ALTER TABLE `tb_variable`
  ADD PRIMARY KEY (`id_variable`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_assessment`
--
ALTER TABLE `tb_assessment`
  MODIFY `id_assessment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_instrumen`
--
ALTER TABLE `tb_detail_instrumen`
  MODIFY `id_detail_instrumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=830;

--
-- AUTO_INCREMENT untuk tabel `tb_instrumen`
--
ALTER TABLE `tb_instrumen`
  MODIFY `id_instrumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT untuk tabel `tb_jawaban_instrumen`
--
ALTER TABLE `tb_jawaban_instrumen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT untuk tabel `tb_judul_instrumen`
--
ALTER TABLE `tb_judul_instrumen`
  MODIFY `id_judul_instrumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tb_parameter`
--
ALTER TABLE `tb_parameter`
  MODIFY `id_parameter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_responden`
--
ALTER TABLE `tb_responden`
  MODIFY `id_responden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_variable`
--
ALTER TABLE `tb_variable`
  MODIFY `id_variable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_assessment`
--
ALTER TABLE `tb_assessment`
  ADD CONSTRAINT `tb_assessment_id_instrumen_foreign` FOREIGN KEY (`id_instrumen`) REFERENCES `tb_instrumen` (`id_instrumen`),
  ADD CONSTRAINT `tb_assessment_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tb_detail_instrumen`
--
ALTER TABLE `tb_detail_instrumen`
  ADD CONSTRAINT `tb_detail_instrumen_id_instrumen_foreign` FOREIGN KEY (`id_instrumen`) REFERENCES `tb_instrumen` (`id_instrumen`),
  ADD CONSTRAINT `tb_detail_instrumen_id_parameter_foreign` FOREIGN KEY (`id_parameter`) REFERENCES `tb_parameter` (`id_parameter`);

--
-- Ketidakleluasaan untuk tabel `tb_instrumen`
--
ALTER TABLE `tb_instrumen`
  ADD CONSTRAINT `tb_instrumen_id_judul_instrumen_foreign` FOREIGN KEY (`id_judul_instrumen`) REFERENCES `tb_judul_instrumen` (`id_judul_instrumen`),
  ADD CONSTRAINT `tb_instrumen_id_variable_foreign` FOREIGN KEY (`id_variable`) REFERENCES `tb_variable` (`id_variable`);

--
-- Ketidakleluasaan untuk tabel `tb_jawaban_instrumen`
--
ALTER TABLE `tb_jawaban_instrumen`
  ADD CONSTRAINT `tb_jawaban_instrumen_id_detail_instrumen_foreign` FOREIGN KEY (`id_detail_instrumen`) REFERENCES `tb_detail_instrumen` (`id_detail_instrumen`),
  ADD CONSTRAINT `tb_jawaban_instrumen_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tb_responden`
--
ALTER TABLE `tb_responden`
  ADD CONSTRAINT `tb_responden_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
