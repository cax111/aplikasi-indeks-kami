<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crudJudulInstrument extends Model
{
    protected $table = "tb_judul_instrumen";
    protected $primaryKey = "id_judul_instrumen";
    public $timestamps = false;

    public function instrument(){
    	return $this->hasMany('App\crudJudulInstrument', $this->primaryKey);
    }
}
