<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\crudInstrument;
use App\crudIdentitas;
use App\crudDetailInstrument;
use App\crudJudulInstrument;
use App\crudParameter;
use App\crudJawabanInstrument;

class instrumentController extends Controller
{
    public $halaman = 'kelolaVariable';
    //tampil ini untuk dihalaman assessment
    public function tampilInstrument($idVar){
      // cek apakah pengguna ini tipe user
      if(\Auth::user()->role != "user"){
        redirect()->to('/')->send();
      }
      // ---------------- selesai cek -------------------
      // cek apakah pengguna sudah mengisi identitasnya atau belum
      $id = \Auth::user()->id_user;
      $identitas = crudIdentitas::where('id_user',$id)->get();
      if(empty($identitas)){
        redirect()->to('/identitas')->send();
      }
      // ------------- selesai cek ----------------------

        $pertanyaan = crudInstrument::select('*')
                                    ->join('tb_variable','tb_variable.id_variable','tb_instrumen.id_variable')
                                    ->join('tb_judul_instrumen','tb_judul_instrumen.id_judul_instrumen','tb_instrumen.id_judul_instrumen')
                                    ->where('tb_instrumen.id_variable',$idVar)->orderBy('tb_judul_instrumen.id_judul_instrumen', 'ASC')->get();
        $pertanyaan->load(['detailInstrument'=>function($query){
                                                    return $query->select('*')->join('tb_parameter','tb_parameter.id_parameter','tb_detail_instrumen.id_parameter');
                                                }
                        ]);
        return view('halamanAssessment.assessment',['pertanyaan'=>$pertanyaan, 'halaman'=>'assessment']);
    }
    //tampil ini untuk dihalaman assessment
    public function lihatAsesmen($idCampur){

      $pisahData = explode('-',$idCampur);
      // cek apakah pengguna ini tipe user
      if(\Auth::user()->role != "user"){
        redirect()->to('/')->send();
      }
      $id = \Auth::user()->id_user;
        $pertanyaan = crudInstrument::select('*')
                                    ->join('tb_variable','tb_variable.id_variable','tb_instrumen.id_variable')
                                    ->join('tb_judul_instrumen','tb_judul_instrumen.id_judul_instrumen','tb_instrumen.id_judul_instrumen')
                                    ->where('tb_instrumen.id_variable',$pisahData[0])->orderBy('tb_judul_instrumen.id_judul_instrumen', 'ASC')->get();
        $pertanyaan->load(['detailInstrument'=>function($query){
                                                    return $query->select('*')->join('tb_parameter','tb_parameter.id_parameter','tb_detail_instrumen.id_parameter');
                                                }
                        ]);
        $jawaban = crudJawabanInstrument::select('*')
                                        ->join(\DB::raw('(tb_detail_instrumen inner join
                                              (tb_instrumen inner join tb_variable on tb_variable.id_variable=tb_instrumen.id_variable
                                              inner join tb_judul_instrumen on tb_judul_instrumen.id_judul_instrumen=tb_instrumen.id_judul_instrumen)
                                              on tb_detail_instrumen.id_instrumen=tb_instrumen.id_instrumen
                                              inner join tb_parameter on tb_parameter.id_parameter=tb_detail_instrumen.id_parameter)'),
                                              'tb_detail_instrumen.id_detail_instrumen',
                                              'tb_jawaban_instrumen.id_detail_instrumen')
                                        ->where('tb_variable.id_variable',$pisahData[0])
                                        ->where('tb_jawaban_instrumen.id_jawaban_instrumen',$pisahData[1])
                                        ->where('tb_jawaban_instrumen.id_user',$id)->orderBy('tb_jawaban_instrumen.created_at', 'ASC')->get();

        $jawabanParameter = [];
        $id_jawaban = null;
        $j = 0;
        for($i=0;$i<count($jawaban);$i++){
          $jawabanParameter[$j]['id_jawaban_instrumen'] = $jawaban[$i]['id_jawaban_instrumen'];
          $id_jawaban = $jawaban[$i]['id_jawaban_instrumen'];
          $jawabanParameter[$j]['id_detail_instrumen'] = $jawaban[$i]['id_detail_instrumen'];
          $jawabanParameter[$j]['id_instrumen'] = $jawaban[$i]['id_instrumen'];
          $jawabanParameter[$j]['isi_parameter'] = $jawaban[$i]['isi_parameter'];
          $jawabanParameter[$j]['bobot_parameter'] = $jawaban[$i]['bobot_parameter'];
          $j++;
        }
        return view('halamanAssessment.lihat-asesmen',['pertanyaan'=>$pertanyaan,'id_jawaban'=>$id_jawaban, 'jawabanParameter'=>$jawabanParameter, 'halaman'=>'assessment']);
    }
    //tampil ini untuk dihalaman assessment
    public function ubahAsesmen($idCampur){

      $pisahData = explode('-',$idCampur);
      // cek apakah pengguna ini tipe user
      if(\Auth::user()->role != "user"){
        redirect()->to('/')->send();
      }
      $id = \Auth::user()->id_user;
        $pertanyaan = crudInstrument::select('*')
                                    ->join('tb_variable','tb_variable.id_variable','tb_instrumen.id_variable')
                                    ->join('tb_judul_instrumen','tb_judul_instrumen.id_judul_instrumen','tb_instrumen.id_judul_instrumen')
                                    ->where('tb_instrumen.id_variable',$pisahData[0])->orderBy('tb_judul_instrumen.id_judul_instrumen', 'ASC')->get();
        $pertanyaan->load(['detailInstrument'=>function($query){
                                                    return $query->select('*')->join('tb_parameter','tb_parameter.id_parameter','tb_detail_instrumen.id_parameter');
                                                }
                        ]);
        $jawaban = crudJawabanInstrument::select('*')
                                        ->join(\DB::raw('(tb_detail_instrumen inner join
                                              (tb_instrumen inner join tb_variable on tb_variable.id_variable=tb_instrumen.id_variable
                                              inner join tb_judul_instrumen on tb_judul_instrumen.id_judul_instrumen=tb_instrumen.id_judul_instrumen)
                                              on tb_detail_instrumen.id_instrumen=tb_instrumen.id_instrumen
                                              inner join tb_parameter on tb_parameter.id_parameter=tb_detail_instrumen.id_parameter)'),
                                              'tb_detail_instrumen.id_detail_instrumen',
                                              'tb_jawaban_instrumen.id_detail_instrumen')
                                        ->where('tb_variable.id_variable',$pisahData[0])
                                        ->where('tb_jawaban_instrumen.id_jawaban_instrumen',$pisahData[1])
                                        ->where('tb_jawaban_instrumen.id_user',$id)->orderBy('tb_jawaban_instrumen.created_at', 'ASC')->get();

        $jawabanParameter = [];
        $id_jawaban = null;
        $j = 0;
        for($i=0;$i<count($jawaban);$i++){
          $jawabanParameter[$j]['id_jawaban_instrumen'] = $jawaban[$i]['id_jawaban_instrumen'];
          $id_jawaban = $jawaban[$i]['id_jawaban_instrumen'];
          $jawabanParameter[$j]['id_detail_instrumen'] = $jawaban[$i]['id_detail_instrumen'];
          $jawabanParameter[$j]['id_instrumen'] = $jawaban[$i]['id_instrumen'];
          $jawabanParameter[$j]['isi_parameter'] = $jawaban[$i]['isi_parameter'];
          $jawabanParameter[$j]['bobot_parameter'] = $jawaban[$i]['bobot_parameter'];
          $j++;
        }
        return view('halamanAssessment.ubah-asesmen',['pertanyaan'=>$pertanyaan,'id_jawaban'=>$id_jawaban, 'jawabanParameter'=>$jawabanParameter, 'halaman'=>'assessment']);
    }
    //tampil ini untuk dihalaman tampil instrument
    public function tampilDataInstrument($id){
        $instruments = crudInstrument::select('*')->where('tb_instrumen.id_variable',$id)->paginate(10);
        return view('halamanInstrumen.tampil-instrument',['instruments'=>$instruments,'id'=>$id, 'halaman'=>$this->halaman]);
    }

    public function hapusInstrument($id){
        $auth = \Auth::user();
        $instrument = crudInstrument::find($id);
        if($auth->role!="admin"){       // fungsi if ini untuk membatasi hanya role admin yang bisa menghapus data user
            return \Redirect('tampil-instrument/'.$id)->withErrors(["gagal" => "Gagal menghapus data, Hanya admin yang diperbolehkan menghapus data."]);
        }elseif($instrument->id_variable <= '6'){
            return \Redirect('tampil-instrument/'.$id)->withErrors(["gagal" => "Gagal menghapus data, Hanya data dengan ID Variablenya lebih dari enam yang bisa dihapus"]);
        }

        try{
            $detailInstrument = crudDetailInstrument::where("id_instrumen",$id);
            $detailInstrument->delete();
            $instrument = crudInstrument::find($id);
            $id_variable = $instrument->id_variable;
            $instrument->delete();
            return \Redirect('tampil-instrument/'.$id_variable);
        }catch(\Exception $e){
            return \Redirect('tampil-instrument/'.$id_variable)->withErrors(["gagal" => "Gagal menghapus data, silakan cek relasi data tersebut."]); //.$e->getMessage() untuk menampilkan pesan error query sql.
        }
    }

    public function tambahInstrument($id){
        $judul_instrument = crudJudulInstrument::all();
        $parameter = crudParameter::all();
        return view('halamanInstrumen.tambah-instrument',['id'=>$id,'judul_instrument'=>$judul_instrument, 'parameter'=>$parameter, 'halaman'=>$this->halaman]);
    }

    public function prosesTambahInstrument(Request $request, $id){
        //proses menghitung jumlah jawaban yang disertakan pada instrument tersebut
        $hitung = count($request->parameter);
        $rules = array(
            'judul_instrument'=>'required',
            'bobot_instrument'=>'required|numeric',
            'isi_instrument'=>'required',
            'parameter'=>'required',
        );
        $validator = \Validator::make($request->all(),$rules);

        if($validator->fails()){
            return back()
                ->withErrors($validator) // tampilkan error input pada halaman login
                ->withInput(); // tampilkan semua input dalam form
        }else{
            try{
                $input = new crudInstrument;
                $input->id_variable            = $id;
                $input->id_judul_instrumen     = $request->get('judul_instrument');
                $input->bobot_instrumen        = $request->get('bobot_instrument');
                $input->isi_instrumen          = $request->get('isi_instrument');
                $input->save();

                echo "<script>alert('berhasil')</script>";
                $cek = crudInstrument::orderBy('id_instrumen', 'desc')->first();
                foreach($request->parameter as $tampil){
                    $input = new crudDetailInstrument;
                    $input->id_instrumen        = $cek->id_instrumen;
                    $input->id_parameter        = $tampil;
                    $input->save();
                    echo "<script>alert('berhasil')</script>";
                }

                return \Redirect('tampil-instrument/'.$id);
            }catch(\Exception $e){
                echo "<script>alert('gagal')</script>";
                return \Redirect('tampil-instrument/'.$id)->withErrors(["gagalInput" => "Gagal Input Data kedalam Database."]);
            }
        }
        //echo "<script>alert('".count($request->parameter)."')</script>";
        //return view('tambah-instrument')->;
    }

    //
    public function ubahInstrument($id){
        $instrument = crudInstrument::select('*')
                                    ->where('id_instrumen',$id)
                                    ->get();
        $detailInstrument = crudDetailInstrument::select('*')
                                    ->where('id_instrumen',$id)
                                    ->get();
        $judul_instrument = crudJudulInstrument::all();
        $parameter = crudParameter::all();
        return view('halamanInstrumen.ubah-instrument',['id'=>$id,'instrument'=>$instrument,'detailInstrument'=>$detailInstrument,'judul_instrument'=>$judul_instrument, 'parameter'=>$parameter, 'halaman'=>$this->halaman]);
    }

    public function prosesUbahInstrument(Request $request, $id){
      //proses menghitung jumlah jawaban yang disertakan pada instrument tersebut
      $hitung = count($request->parameter);
      $rules = array(
        'judul_instrument'=>'required',
        'bobot_instrument'=>'required|numeric',
        'isi_instrument'=>'required',
        'parameter'=>'required',
      );
      $validator = \Validator::make($request->all(),$rules);

      if($validator->fails()){
        return back()
        ->withErrors($validator) // tampilkan error input pada halaman login
        ->withInput(); // tampilkan semua input dalam form
      }else{
        try{
          //ubah data instrumennya...
          $input = crudInstrument::find($id);
          $input->id_judul_instrumen     = $request->get('judul_instrument');
          $input->bobot_instrumen        = $request->get('bobot_instrument');
          $input->isi_instrumen          = $request->get('isi_instrument');
          $input->save();

          //untuk ubah parameter...
          //delete dulu parameternyaa...
          echo "<script>alert('berhasil')</script>";
          $detailInstrument = crudDetailInstrument::select('*')
                                                  ->where('id_instrumen',$id);
          $detailInstrument->delete();

          //input lagi we...
          $cek = crudInstrument::find($id);
          foreach($request->parameter as $tampil){
            $input = new crudDetailInstrument;
            $input->id_instrumen        = $cek->id_instrumen;
            $input->id_parameter        = $tampil;
            $input->save();
          }

          return \Redirect('tampil-detail-instrument/'.$id);
        }catch(\Exception $e){
          echo "<script>alert('gagal')</script>";
          return \Redirect('tampil-detail-instrument/'.$id)->withErrors(["gagalInput" => "Gagal Input Data kedalam Database."]);
        }
      }
    }
}
