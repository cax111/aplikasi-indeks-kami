<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\crudJawabanInstrument;
use App\crudVariable;
use App\crudIdentitas;
use App\crudParameter;
use App\crudSaran;

class jawabanInstrumentController extends Controller
{
    public $halaman = "hasilAssessment";

    public function tampilHasilAssessment(){
      // cek apakah role assessor atau bukan
      if(\Auth::user()->role != "assessor"){
        redirect()->to('/')->send();
      }

      // inisialisasi data  dari model
      $dataJawabanInstrument = crudJawabanInstrument::select('*')
                                                      ->join(\DB::raw('(tb_user inner join tb_responden on tb_responden.id_user=tb_user.id_user)'),'tb_user.id_user','tb_jawaban_instrumen.id_user')
                                                      ->groupBy('tb_jawaban_instrumen.id_jawaban_instrumen')
                                                      ->orderBy('tb_jawaban_instrumen.id_jawaban_instrumen', 'DESC')
                                                      ->get();

      $dataVariableTerisi = crudJawabanInstrument::select('*')
                                                      ->join(\DB::raw('(tb_detail_instrumen inner join (tb_instrumen inner join tb_variable on tb_instrumen.id_variable=tb_variable.id_variable) on tb_instrumen.id_instrumen=tb_detail_instrumen.id_instrumen)'),'tb_detail_instrumen.id_detail_instrumen','tb_jawaban_instrumen.id_detail_instrumen')
                                                      ->groupBy('tb_variable.id_variable')
                                                      ->groupBy('tb_jawaban_instrumen.id_user')
                                                      ->groupBy('tb_jawaban_instrumen.id_jawaban_instrumen')
                                                      ->get();
      $dataVariable = crudVariable::all();


        // foreach($data AS $t){
        //   if($ambilId != $t->id_user){
        //     if(!empty($ambilIdVar)){
        //      if($ambilIdVar[$j] != $t->id_variable){
        //         $ambilIdVar[] = $t->id_variable;
        //         $j++;
        //     }
        //    }else{
        //         $ambilIdVar[] = $t->id_variable;
        //     }
        //   }
        // }

      return view('halamanAssessment.tampil-hasil-assessment',['data'=>$dataJawabanInstrument, 'data2'=>$dataVariable, 'dataVariableTerisi'=>$dataVariableTerisi, 'halaman'=>$this->halaman]);
    }

    public function tampilDetailHasilAssessment($id){
      // cek apakah role assessor atau bukan
      if(\Auth::user()->role != "assessor"){
        redirect()->to('/')->send();
      }
      $dataJawabanInstrument = crudJawabanInstrument::select('*')
                                                    ->join(\DB::raw('(tb_detail_instrumen inner join (tb_instrumen inner join tb_variable on tb_instrumen.id_variable=tb_variable.id_variable) on tb_instrumen.id_instrumen=tb_detail_instrumen.id_instrumen)'),'tb_detail_instrumen.id_detail_instrumen','tb_jawaban_instrumen.id_detail_instrumen')
                                                    ->join(\DB::raw('(tb_user inner join tb_responden on tb_responden.id_user=tb_user.id_user)'),'tb_user.id_user','tb_jawaban_instrumen.id_user')
                                                    ->where('tb_jawaban_instrumen.id_jawaban_instrumen',$id)
                                                    ->orderBy('tb_variable.id_variable', 'ASC')
                                                    ->get();
    $dataSaran = crudJawabanInstrument::select('*')
                                                  ->join('tb_saran','tb_saran.id_jawaban_instrumen','tb_jawaban_instrumen.id_jawaban_instrumen')
                                                  ->where('tb_saran.id_jawaban_instrumen',$id)
                                                  ->get();
      $i = 0;
      $skorVariable = [];
      $skorVariable[] = 0;
      $urutanVariable = [];
      $urutanVariable[] = $dataJawabanInstrument[0]->id_variable;

      //mencari skor semua variable & mencari tingkat kematangan tiap variable
      $totalSkorTK2=[];
      $totalSkorTK3=[];
      $totalSkorTK4=[];
      $totalSkorTK5=[];
      $totalSkorTK2[]=0;
      $totalSkorTK3[]=0;
      $totalSkorTK4[]=0;
      $totalSkorTK5[]=0;

      $tingkat_Kematangan=[];

      $skorSuplemen1 = 0;
      $skorSuplemen2 = 0;
      $skorSuplemen3 = 0;

      foreach($dataJawabanInstrument AS $skor){
        if($skor->id_judul_instrumen > 10 && $skor->id_judul_instrumen < 14){
          if($skor->id_judul_instrumen == 11){
            $skorSuplemen1 += $skor->skor;
          }elseif($skor->id_judul_instrumen == 12){
            $skorSuplemen2 += $skor->skor;
          }else{
            $skorSuplemen3 += $skor->skor;
          }
        }

          if($urutanVariable[$i] == $skor->id_variable){
            $skorVariable[$i] += $skor->skor;
            //tingkat kematangan
            if($skor->tingkat_kematangan==2){
              $totalSkorTK2[$i]+=$skor->skor;
            }
            if($skor->tingkat_kematangan==3){
              $totalSkorTK3[$i]+=$skor->skor;
            }
            if($skor->tingkat_kematangan==4){
              $totalSkorTK4[$i]+=$skor->skor;
            }
            if($skor->tingkat_kematangan==5){
              $totalSkorTK5[$i]+=$skor->skor;
            }
          }else{
            $i++;
            $skorVariable[] += $skor->skor;
            $urutanVariable[] = $skor->id_variable;
            //tingkat kematangan
            if($skor->tingkat_kematangan==2){
              $totalSkorTK2[]+=$skor->skor;
            }else{
              $totalSkorTK2[]=0;
            }
            if($skor->tingkat_kematangan==3){
              $totalSkorTK3[]+=$skor->skor;
            }else{
                $totalSkorTK3[]=0;
            }
            if($skor->tingkat_kematangan==4){
              $totalSkorTK4[]+=$skor->skor;
            }else{
                $totalSkorTK4[]=0;
            }
            if($skor->tingkat_kematangan==5){
              $totalSkorTK5[]+=$skor->skor;
            }else{
                $totalSkorTK5[]=0;
            }
          }
      }

        $skorku = array_combine($urutanVariable,$skorVariable);
        $skorTK2ku = array_combine($urutanVariable,$totalSkorTK2);
        $skorTK3ku = array_combine($urutanVariable,$totalSkorTK3);
        $skorTK4ku = array_combine($urutanVariable,$totalSkorTK4);
        $skorTK5ku = array_combine($urutanVariable,$totalSkorTK5);
        $skorSemua = 0;
        $dataVariable = crudVariable::all();
        $kategorise = "";
        $hasilEvaluasiAkhir = "";
        //mencari kategori SE
        if(!empty($skorku[1])){
          if($skorku[1]>=10 && $skorku[1]<=15){
            $kategorise="Rendah";
          }elseif($skorku[1]>=16 && $skorku[1]<=34){
            $kategorise="Tinggi";
          }else{
            $kategorise="Strategis";
          }
        }

        foreach($dataVariable AS $tampil2){
          if($tampil2->id_variable==2){
            $tk_skorMinTingkat2=(4*2)+(4*1);
            $tk_skorPencapaianTingkat2=(8*2)+(5*4);
            $tk_ambangBatasTingkat3=0.8*((8*3)+(5*6));
            $tk_validitasTingkat3='no';
            $tk_skorMinTingkat3=(2*2)+(1*4);
            $tk_skorPencapaianTingkat3=(2*4)+(1*6);
            $tk_ambangBatasTingkat4=(1*4)+(2*6);
            $tk_validitasTingkat4='no';
            $tk_skorMinTingkat4=(2*6)+(4*3);
            $tk_skorPencapaianTingkat4=6*9;
          }elseif($tampil2->id_variable==3){
            $tk_skorMinTingkat2=(4*2)+(6*1);
            $tk_skorPencapaianTingkat2=(10*2);
            $tk_ambangBatasTingkat3=0.8*10*3;
            $tk_validitasTingkat3='no';
            $tk_skorMinTingkat3=(2*2)+(0*4);
            $tk_skorPencapaianTingkat3=(2*4);
            $tk_ambangBatasTingkat4=(1*4)+(1*6);
            $tk_validitasTingkat4='no';
            $tk_skorMinTingkat4=(2*4);
            $tk_skorPencapaianTingkat4=2*6;
            $tk_ambangBatasTingkat5=2*6;
            $tk_validitasTingkat5='no';
            $tk_skorMinTingkat5=(2*6);
            $tk_skorPencapaianTingkat5=2*9;
          }elseif($tampil2->id_variable==4){
            $tk_skorMinTingkat2=(4*2)+(7*1);
            $tk_skorPencapaianTingkat2=(10*2)+(1*4);
            $tk_ambangBatasTingkat3=0.8*((8*3)+(3*6));
            $tk_validitasTingkat3='no';
            $tk_skorMinTingkat3=(4*3)+(2*2)+(5*4)+(1*3)+(1*6);
            $tk_skorPencapaianTingkat3=(4*3)+(2*4)+(5*6)+(2*6);
            $tk_ambangBatasTingkat4=(4*3)+(1*4)+(5*6)+(1*6)+(2*9);
            $tk_validitasTingkat4='no';
            $tk_skorMinTingkat4=(2*6)+(1*3);
            $tk_skorPencapaianTingkat4=3*9;
            $tk_ambangBatasTingkat5=3*9;
            $tk_validitasTingkat5='no';
            $tk_skorMinTingkat5=(2*6);
            $tk_skorPencapaianTingkat5=2*9;
          }elseif($tampil2->id_variable==5){
            $tk_skorMinTingkat2=(4*2)+(17*1);
            $tk_skorPencapaianTingkat2=(21*2)+(5*4);
            $tk_ambangBatasTingkat3=0.8*((23*3)+(6*6));
            $tk_validitasTingkat3='no';
            $tk_skorMinTingkat3=(3*2)+(2*4)+(1*3)+(3*6);
            $tk_skorPencapaianTingkat3=(2*4)+(3*6)+(4*6);
            $tk_ambangBatasTingkat4=(1*4)+(4*6)+(1*6)+(3*9);
          }elseif($tampil2->id_variable==6){
            $tk_skorMinTingkat2=(4*2)+(10*1);
            $tk_skorPencapaianTingkat2=14*2;
            $tk_ambangBatasTingkat3=0.8*14*3;
            $tk_validitasTingkat3='no';
            $tk_skorMinTingkat3=(2*2)+(9*4);
            $tk_skorPencapaianTingkat3=(2*4)+(9*6);
            $tk_ambangBatasTingkat4=(1*4)+(10*6);
            $tk_validitasTingkat4='no';
            $tk_angkaValiditasTingkat4=(1*6);
            $tk_skorMinTingkat4=(1*9);
            $tk_skorPencapaianTingkat4=1*9;
          }
          //skor semua variable
          if(!empty($skorku[$tampil2->id_variable]) && $tampil2->id_variable > 1 && $tampil2->id_variable < 7){
            $skorSemua += $skorku[$tampil2->id_variable];
          }
          //tingkat kematangan
          if(!empty($skorTK2ku[$tampil2->id_variable]) && $tampil2->id_variable > 1 && $tampil2->id_variable < 7){
          //tingkat kematangan 2
          if($skorTK2ku[$tampil2->id_variable]>=$tk_skorMinTingkat2){
            $tingkat_Kematangan[$tampil2->id_variable]="I+";

            if($skorTK2ku[$tampil2->id_variable]>=$tk_skorPencapaianTingkat2){
              $tingkat_Kematangan[$tampil2->id_variable]="II";
            }

            //validitas tingkat 3
            if($skorTK2ku[$tampil2->id_variable]>=$tk_ambangBatasTingkat3){
              $tk_validitasTingkat3="yes";
            }else{
              $tk_validitasTingkat3="no";
            }

            //tingkat kematangan 3
            if($tk_validitasTingkat3=="yes"){
              if($skorTK3ku[$tampil2->id_variable]>=$tk_skorMinTingkat3){
                $tingkat_Kematangan[$tampil2->id_variable]="II+";
                if($skorTK3ku[$tampil2->id_variable]>=$tk_skorPencapaianTingkat3){
                  $tingkat_Kematangan[$tampil2->id_variable]="III";
                }

                //validitas tingkat 4
                if($skorTK3ku[$tampil2->id_variable]>=$tk_ambangBatasTingkat4){
                  $tk_validitasTingkat4="yes";
                }else{
                  $tk_validitasTingkat4="no";
                }

                //tingkat kematangan 4
                if($tk_validitasTingkat4=="yes"){
                  if($skorTK4ku[$tampil2->id_variable]>=$tk_skorMinTingkat4){
                    $tingkat_Kematangan[$tampil2->id_variable]="III+";
                    if($skorTK4ku[$tampil2->id_variable]>=$tk_skorPencapaianTingkat4){
                      $tingkat_Kematangan[$tampil2->id_variable]="IV";
                    }

                    //validitas tingkat 5
                    if($skorTK4ku[$tampil2->id_variable]>=$tk_ambangBatasTingkat5){
                      $tk_validitasTingkat5="yes";
                    }else{
                      $tk_validitasTingkat5="no";
                    }

                    //tingkat kematangan 5
                    if($tk_validitasTingkat5=="yes"){
                      if($skorTK5ku[$tampil2->id_variable]>=$tk_skorMinTingkat5){
                        $tingkat_Kematangan[$tampil2->id_variable]="IV+";
                        if($skorTK5ku[$tampil2->id_variable]>=$tk_skorPencapaianTingkat5){
                          $tingkat_Kematangan[$tampil2->id_variable]="V";
                        }
                      }
                    }
                  }
                }
              }
            }
            }else{
              $tingkat_Kematangan[$tampil2->id_variable]="I";
            }
          }
        }
        //mencari Hasil Evaluasi Akhir
        if(!empty($skorSemua)){
          if($kategorise == "Rendah"){
            if($skorSemua<=174){
               $hasilEvaluasiAkhir = "Tidak Layak";
            }elseif($skorSemua<=312){
               $hasilEvaluasiAkhir = "Pemenuhan Kerangka Dasar";
            }elseif($skorSemua<=535){
               $hasilEvaluasiAkhir = "Cukup Baik";
            }elseif($skorSemua<=645){
               $hasilEvaluasiAkhir = "Baik";
            }
          }elseif($kategorise == "Tinggi"){
            if($skorSemua<=272){
               $hasilEvaluasiAkhir = "Tidak Layak";
            }elseif($skorSemua<=455){
               $hasilEvaluasiAkhir = "Pemenuhan Kerangka Dasar";
            }elseif($skorSemua<=583){
               $hasilEvaluasiAkhir = "Cukup Baik";
            }elseif($skorSemua<=645){
               $hasilEvaluasiAkhir = "Baik";
            }
          }elseif($kategorise == "Strategis"){
            if($skorSemua<=333){
               $hasilEvaluasiAkhir = "Tidak Layak";
            }elseif($skorSemua<=535){
               $hasilEvaluasiAkhir = "Pemenuhan Kerangka Dasar";
            }elseif($skorSemua<=609){
               $hasilEvaluasiAkhir = "Cukup Baik";
            }elseif($skorSemua<=645){
               $hasilEvaluasiAkhir = "Baik";
            }
          }
        }
        $defuzifikasi=$this->metodeFuzzyMamdani($skorku,$skorSemua);

        return view('halamanAssessment.tampil-detail-hasil-assessment',['data'=>$dataJawabanInstrument,
                                                                    'data2'=>$dataVariable,
                                                                    'dataSaran'=>$dataSaran,
                                                                    'kategorise'=>$kategorise,
                                                                    'hasilEvaluasiAkhir'=>$hasilEvaluasiAkhir,
                                                                    'skorSemua'=>$skorSemua,
                                                                    'urutanSkor'=>$urutanVariable,
                                                                    'skorku'=>$skorku,
                                                                    'skorSuplemen1'=>($skorSuplemen1/27),
                                                                    'skorSuplemen2'=>($skorSuplemen2/10),
                                                                    'skorSuplemen3'=>($skorSuplemen3/16),
                                                                    'tingkat_Kematangan'=>$skorTK2ku,
                                                                    'tingkat_Kematangan2'=>$tingkat_Kematangan,
                                                                    'get'=>$id,
                                                                    'defuzifikasi'=>$defuzifikasi,
                                                                    'halaman'=>$this->halaman]);
    }

    public function prosesPengajuanPeninjauan($id_jawaban){
        $id = \Auth::user()->id_user;
        try{
          //-----------------------------------------------------------
          // input database
          $input = crudJawabanInstrument::where('id_user',$id)->where('id_jawaban_instrumen',$id_jawaban)->update(['status_review' => 1]);

          return \Redirect('variable/'.$id_jawaban)->with(["alert" => "Berhasil Input Data kedalam Database."]);
        }catch(\Exception $e){
          return \Redirect('variable/'.$id_jawaban)->withErrors(["alert" => "Gagal Input Data kedalam Database.".$e->getMessage()]);
        }
    }
    public function prosesIsiJawabanInstrument(Request $request){
        $id = \Auth::user()->id_user;
        $instruments = crudParameter::all();
        foreach($request->status AS $status){
          $cekisi = 0;
          foreach($instruments AS $cek){
            $pisahData = explode('-',$status);
            $parameter = $pisahData[1];
            if($parameter==$cek->isi_parameter){
              $cekisi = 1;
            }
          }
          if($cekisi == 0){
            return back()->withErrors(["gagal" => "gagal Input Data kedalam Database, silakan cek kembali pengisian status instrumen anda."]);
          }
        }
        $rules = array(
            'status'=>'required',
        );
        $validator = \Validator::make($request->all(),$rules);

        if($validator->fails()){
            return back()
                ->withErrors($validator) // tampilkan error input pada halaman login
                ->withInput(); // tampilkan semua input dalam form
        }else{
            try{
                $id_jawaban_instrument = 0;
                $ambilStatus = crudJawabanInstrument::where('id_user',$id)->orderBy('id_jawaban_instrumen', 'DESC')->first();
                $ambilId = crudJawabanInstrument::orderBy('id_jawaban_instrumen', 'DESC')->first();
                if(empty($ambilId)){
                  $id_jawaban_instrument++;
                }elseif($ambilStatus->status_review==1){
                  $id_jawaban_instrument = $ambilId['id_jawaban_instrumen'] + 1;
                }else{
                  $id_jawaban_instrument = $ambilId->id_jawaban_instrumen;
                }
                // perhitungan untuk mendapatkan skor di pertanyaan tahap 3
                $totalSkor=0;
                $p_tahap1=0;
                $p_tahap2=0;

                foreach($request->status AS $status){
                  $pisahData = explode('-',$status);
                  $id_detail_instrument = $pisahData[0];
                  $nilai = $pisahData[2];
                  $bobot = $pisahData[3];
                  $tingkat_Kematangan = $pisahData[4];

                  if($bobot!=3){
                    $totalSkor += $nilai*$bobot;
                  }
                  if($bobot==1){
                    $p_tahap1++;
                  }elseif($bobot==2){
                    $p_tahap2++;
                  }

                }
                //-----------------------------------------------------------
                // input database
                foreach($request->status AS $status){
                  $pisahData = explode('-',$status);
                  $id_detail_instrument = $pisahData[0];
                  $nilai = $pisahData[2];
                  $bobot = $pisahData[3];
                  $skor = $nilai*$bobot;
                  if($bobot==3 && $totalSkor<((2*$p_tahap1)+(4*$p_tahap2))){
                    $skor = 0;
                  }
                  $input = new crudJawabanInstrument;
                  $input->id_jawaban_instrumen  = $id_jawaban_instrument;
                  $input->id_detail_instrumen  = $id_detail_instrument;
                  $input->id_user   = $id;
                  $input->skor      = $skor;
                  $input->save();
                }
                return \Redirect('variable/'.$id_jawaban_instrument)->with(["alert" => "Berhasil Input Data kedalam Database."]);
            }catch(\Exception $e){
                return \Redirect('variable/'.$id_jawaban_instrument)->withErrors(["alert" => "Gagal Input Data kedalam Database.".$e->getMessage()]);
            }
        }
    }
    public function prosesUbahJawabanInstrument(Request $request){
        $id = \Auth::user()->id_user;
        $instruments = crudParameter::all();
        foreach($request->status AS $status){
          $cekisi = 0;
          foreach($instruments AS $cek){
            $pisahData = explode('-',$status);
            $parameter = $pisahData[1];
            if($parameter==$cek->isi_parameter){
              $cekisi = 1;
            }
          }
          if($cekisi == 0){
            return back()->withErrors(["gagal" => "gagal Input Data kedalam Database, silakan cek kembali pengisian status instrumen anda."]);
          }
        }
        $rules = array(
            'status'=>'required',
        );
        $validator = \Validator::make($request->all(),$rules);

        if($validator->fails()){
            return back()
                ->withErrors($validator) // tampilkan error input pada halaman login
                ->withInput(); // tampilkan semua input dalam form
        }else{
            try{
                $id_jawaban_instrument = 0;
                $ambilId = crudJawabanInstrument::where('id_user',$id)->orderBy('id', 'DESC')->first();
                if(empty($ambilId)){
                  $id_jawaban_instrument++;
                }elseif($ambilId->status_review==1){
                  $id_jawaban_instrument = $ambilId['id_jawaban_instrumen'] + 1;
                }else{
                  $id_jawaban_instrument = $ambilId->id_jawaban_instrumen;
                }
                // perhitungan untuk mendapatkan skor di pertanyaan tahap 3
                $totalSkor=0;
                $p_tahap1=0;
                $p_tahap2=0;

                foreach($request->status AS $status){
                  $pisahData = explode('-',$status);
                  $id_detail_instrument = $pisahData[0];
                  $nilai = $pisahData[2];
                  $bobot = $pisahData[3];
                  $tingkat_Kematangan = $pisahData[4];

                  if($bobot!=3){
                    $totalSkor += $nilai*$bobot;
                  }
                  if($bobot==1){
                    $p_tahap1++;
                  }elseif($bobot==2){
                    $p_tahap2++;
                  }

                }
                //-----------------------------------------------------------
                // input database
                foreach($request->status AS $status){
                  echo "<br>".$status."<br>";
                  $pisahData = explode('-',$status);
                  $id_detail_instrument = $pisahData[0];
                  $id_detail_instrument_old = $pisahData[6];
                  $nilai = $pisahData[2];
                  $bobot = $pisahData[3];
                  $id_jawaban_instrument = $pisahData[5];
                  $skor = $nilai*$bobot;
                  if($bobot==3 && $totalSkor<((2*$p_tahap1)+(4*$p_tahap2))){
                    $skor = 0;
                  }
                  $input = crudJawabanInstrument::where('id_user',$id)->where('id_detail_instrumen',$id_detail_instrument_old)->where('id_jawaban_instrumen',$id_jawaban_instrument)->first();
                  $input->id_jawaban_instrumen  = $id_jawaban_instrument;
                  $input->id_detail_instrumen  = $id_detail_instrument;
                  $input->id_user   = $id;
                  $input->skor      = $skor;
                  $input->save();
                }
                return \Redirect('variable/'.$id_jawaban_instrument)->with(["alert" => "Berhasil Input Data kedalam Database."]);
            }catch(\Exception $e){
                return back()->withErrors(["alert" => "Gagal Input Data kedalam Database. ".$e->getMessage()]);
            }
        }
    }

    public function metodeFuzzyMamdani($skorVariable,$skorSemua){
      if(count($skorVariable)<6){
        return 0;
      }
      //data training
      $dataTataKelola = [29,38,38,71,36,35,32,75,70,38,98,30,39,$skorVariable[2]];
      $dataRisiko = [24,42,40,48,36,37,32,48,39,48,58,25,21,$skorVariable[3]];
      $dataKerangkaKerja = [73,84,86,80,76,78,90,126,108,95,145,48,114,$skorVariable[4]];
      $dataPengelolaanAset = [89,94,90,113,86,94,102,116,124,90,142,56,112,$skorVariable[5]];
      $dataTeknologi = [82,56,64,71,55,74,80,92,87,60,91,47,53,$skorVariable[6]];
      $dataHasil = [297,314,318,383,289,318,336,457,428,331,534,206,339,$skorSemua];

      //mencari batas bawah dan atas dan pengurangan batas

      $batasBawahTK = min($dataTataKelola);
      $batasAtasTK = max($dataTataKelola);
      $penguranganBatasTK = $batasAtasTK-$batasBawahTK;

      $batasBawahRisiko = min($dataRisiko);
      $batasAtasRisiko = max($dataRisiko);
      $penguranganBatasRisiko = $batasAtasRisiko-$batasBawahRisiko;

      $batasBawahKK = min($dataKerangkaKerja);
      $batasAtasKK = max($dataKerangkaKerja);
      $penguranganBatasKK = $batasAtasKK-$batasBawahKK;

      $batasBawahPA = min($dataPengelolaanAset);
      $batasAtasPA = max($dataPengelolaanAset);
      $penguranganBatasPA = $batasAtasPA-$batasBawahPA;

      $batasBawahTeknologi = min($dataTeknologi);
      $batasAtasTeknologi = max($dataTeknologi);
      $penguranganBatasTeknologi = $batasAtasTeknologi-$batasBawahTeknologi;

      $batasBawahHasil = min($dataHasil);
      $batasAtasHasil = max($dataHasil);
      $penguranganBatasHasil = $batasAtasHasil-$batasBawahHasil;
      //=======================================================================================================================

      //mencari derajat Keanggotaan Rendah dan Tinggi

      $derajatKeanggotaanRendahTK = [];
      $derajatKeanggotaanTinggiTK = [];
      $derajatKeanggotaanRendahRisiko = [];
      $derajatKeanggotaanTinggiRisiko = [];
      $derajatKeanggotaanRendahKK = [];
      $derajatKeanggotaanTinggiKK = [];
      $derajatKeanggotaanRendahPA = [];
      $derajatKeanggotaanTinggiPA = [];
      $derajatKeanggotaanRendahTeknologi = [];
      $derajatKeanggotaanTinggiTeknologi = [];
      $derajatKeanggotaanRendahHasil = [];
      $derajatKeanggotaanTinggiHasil = [];

      for($i=0;$i<count($dataTataKelola);$i++){
        $derajatKeanggotaanRendah = ($batasAtasTK-$dataTataKelola[$i])/$penguranganBatasTK;
        $derajatKeanggotaanTinggi = ($dataTataKelola[$i]-$batasBawahTK)/$penguranganBatasTK;

        $derajatKeanggotaanRendahTK[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiTK[] = round($derajatKeanggotaanTinggi, 2);

      }

      for($i=0;$i<count($dataRisiko);$i++){
        $derajatKeanggotaanRendah = ($batasAtasRisiko-$dataRisiko[$i])/$penguranganBatasRisiko;
        $derajatKeanggotaanTinggi = ($dataRisiko[$i]-$batasBawahRisiko)/$penguranganBatasRisiko;

        $derajatKeanggotaanRendahRisiko[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiRisiko[] = round($derajatKeanggotaanTinggi, 2);

      }

      for($i=0;$i<count($dataKerangkaKerja);$i++){
        $derajatKeanggotaanRendah = ($batasAtasKK-$dataKerangkaKerja[$i])/$penguranganBatasKK;
        $derajatKeanggotaanTinggi = ($dataKerangkaKerja[$i]-$batasBawahKK)/$penguranganBatasKK;

        $derajatKeanggotaanRendahKK[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiKK[] = round($derajatKeanggotaanTinggi, 2);

      }

      for($i=0;$i<count($dataPengelolaanAset);$i++){
        $derajatKeanggotaanRendah = ($batasAtasPA-$dataPengelolaanAset[$i])/$penguranganBatasPA;
        $derajatKeanggotaanTinggi = ($dataPengelolaanAset[$i]-$batasBawahPA)/$penguranganBatasPA;

        $derajatKeanggotaanRendahPA[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiPA[] = round($derajatKeanggotaanTinggi, 2);

      }

      for($i=0;$i<count($dataTeknologi);$i++){
        $derajatKeanggotaanRendah = ($batasAtasTeknologi-$dataTeknologi[$i])/$penguranganBatasTeknologi;
        $derajatKeanggotaanTinggi = ($dataTeknologi[$i]-$batasBawahTeknologi)/$penguranganBatasTeknologi;

        $derajatKeanggotaanRendahTeknologi[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiTeknologi[] = round($derajatKeanggotaanTinggi, 2);

      }

      for($i=0;$i<count($dataHasil);$i++){
        $derajatKeanggotaanRendah = ($batasAtasHasil-$dataHasil[$i])/$penguranganBatasHasil;
        $derajatKeanggotaanTinggi = ($dataHasil[$i]-$batasBawahHasil)/$penguranganBatasHasil;

        $derajatKeanggotaanRendahHasil[] = round($derajatKeanggotaanRendah, 2);
        $derajatKeanggotaanTinggiHasil[] = round($derajatKeanggotaanTinggi, 2);

      }
      //=======================================================================================================================

      //mencari rules berdasarkan data Training

      $rules = [];
      for($i=0;$i<count($dataTataKelola);$i++){
        if(max($derajatKeanggotaanRendahTK[$i], $derajatKeanggotaanTinggiTK[$i])==$derajatKeanggotaanTinggiTK[$i]){
          $rules[$i][0] = "tinggi";
        }else{
          $rules[$i][0] = "rendah";
        };
        if(max($derajatKeanggotaanRendahRisiko[$i], $derajatKeanggotaanTinggiRisiko[$i])==$derajatKeanggotaanTinggiRisiko[$i]){
          $rules[$i][1] = "tinggi";
        }else{
          $rules[$i][1] = "rendah";
        };
        if(max($derajatKeanggotaanRendahKK[$i], $derajatKeanggotaanTinggiKK[$i])==$derajatKeanggotaanTinggiKK[$i]){
          $rules[$i][2] = "tinggi";
        }else{
          $rules[$i][2] = "rendah";
        };
        if(max($derajatKeanggotaanRendahPA[$i], $derajatKeanggotaanTinggiPA[$i])==$derajatKeanggotaanTinggiPA[$i]){
          $rules[$i][3] = "tinggi";
        }else{
          $rules[$i][3] = "rendah";
        };
        if(max($derajatKeanggotaanRendahTeknologi[$i], $derajatKeanggotaanTinggiTeknologi[$i])==$derajatKeanggotaanTinggiTeknologi[$i]){
          $rules[$i][4] = "tinggi";
        }else{
          $rules[$i][4] = "rendah";
        };
        if(max($derajatKeanggotaanRendahHasil[$i], $derajatKeanggotaanTinggiHasil[$i])==$derajatKeanggotaanTinggiHasil[$i]){
          $rules[$i][5] = "tinggi";
        }else{
          $rules[$i][5] = "rendah";
        };
      }
      //=======================================================================================================================

      //proses implikasi (mencari nilai dari setiap variable berdasarkan rules)

      $implikasi = [];
      $simpanImplikasi = [];
      $plus = 0;
      for($k=0;$k<count($rules);$k++){
        for($i=0;$i<count($rules);$i++){
          for($j=0;$j<count($rules[$i]);$j++){
            if($rules[$i][0]=="rendah"){
              $implikasi[$k][0] = $derajatKeanggotaanRendahTK[$k];
            }else{
              $implikasi[$k][0] = $derajatKeanggotaanTinggiTK[$k];
            }
            if($rules[$i][1]=="rendah"){
              $implikasi[$k][1] = $derajatKeanggotaanRendahRisiko[$k];
            }else{
              $implikasi[$k][1] = $derajatKeanggotaanTinggiRisiko[$k];
            }
            if($rules[$i][2]=="rendah"){
              $implikasi[$k][2] = $derajatKeanggotaanRendahKK[$k];
            }else{
              $implikasi[$k][2] = $derajatKeanggotaanTinggiKK[$k];
            }
            if($rules[$i][3]=="rendah"){
              $implikasi[$k][3] = $derajatKeanggotaanRendahPA[$k];
            }else{
              $implikasi[$k][3] = $derajatKeanggotaanTinggiPA[$k];
            }
            if($rules[$i][4]=="rendah"){
              $implikasi[$k][4] = $derajatKeanggotaanRendahTeknologi[$k];
            }else{
              $implikasi[$k][4] = $derajatKeanggotaanTinggiTeknologi[$k];
            }
            if($rules[$i][5]=="rendah"){
              $implikasi[$k][5] = $derajatKeanggotaanRendahHasil[$k];
            }else{
              $implikasi[$k][5] = $derajatKeanggotaanTinggiHasil[$k];
            }
          }
          for($j=0;$j<count($rules[$i]);$j++){
              $simpanImplikasi[$plus][$j] = min($implikasi[$k][0],$implikasi[$k][1],$implikasi[$k][2],$implikasi[$k][3],$implikasi[$k][4],$implikasi[$k][5]);
              break;
          }
          $plus++;
        }
      }
      $hasilImplikasi = [];
      $plus = 0;
      $plus2 = 0;
      for($i=0;$i<count($simpanImplikasi);$i++){
        for($j=0;$j<count($simpanImplikasi[$i]);$j++){
          if(($i+1)%count($dataTataKelola)==0){
            $hasilImplikasi[$plus][$plus2] = $simpanImplikasi[$i][$j];
            $plus++;
            $plus2 = -1;
          }else{
            $hasilImplikasi[$plus][$plus2] = $simpanImplikasi[$i][$j];
          }
          $plus2++;
        }
      }
      //=======================================================================================================================

      //proses agregasi (mencari nilai terbesar dari setiap variable)

      $agregasi = [];
      for($i=0;$i<count($hasilImplikasi);$i++){
          $agregasi[$i] = max($hasilImplikasi[$i]);
      }

      //=======================================================================================================================

      //proses Defuzzifikasi metode Mean of Maxima (mengembalikan nilai hasil perhitungan fuzzy dengan metode Mean of Maxima)

      $defuzifikasi = [];
      $defBawah = [];
      $defAtas = [];
      for($i=0;$i<count($rules);$i++){
        $defBawah[$i] = $batasAtasHasil-($penguranganBatasHasil*$agregasi[$i]);
        $defAtas[$i] = ($penguranganBatasHasil*$agregasi[$i])+$batasBawahHasil;
      }
      for($i=0;$i<count($rules);$i++){
        if($rules[$i][5]=="rendah"){
          $defuzifikasi[$i] = ($batasBawahHasil+$defBawah[$i])/2;
        }else{
          $defuzifikasi[$i] = ($batasAtasHasil+$defAtas[$i])/2;
        }
      }

      //=======================================================================================================================

      //web view pada browset

      // echo "<h2>Langkah-langkah Menghitung Metode Fuzzy Mamdani dengan Metode Defuzifikasi Mean Of Maxima (MoM)</h2>";
      // echo"<h3>Langkah 1 : Fuzzifikasi</h3>";
      //
      // echo"<h4>Langkah 1 : Mencari Batas Bawah, Batas Atas dan Pengurangan Batas Dari Setiap Variable</h4>";
      //
      // echo "<p>Batas Bawah Tata Kelola : ".$batasBawahTK."<br/>";
      // echo "Batas Atas Tata Kelola : ".$batasAtasTK."<br/>";
      // echo "Pengurangan Batas Tata Kelola : ".$penguranganBatasTK."<br/></p>";
      //
      // echo "<p>Batas Bawah Risiko : ".$batasBawahRisiko."<br/>";
      // echo "Batas Atas Risiko : ".$batasAtasRisiko."<br/>";
      // echo "Pengurangan Batas Risiko : ".$penguranganBatasRisiko."<br/></p>";
      //
      // echo "<p>Batas Bawah Kerangka Kerja : ".$batasBawahKK."<br/>";
      // echo "Batas Atas Kerangka Kerja : ".$batasAtasKK."<br/>";
      // echo "Pengurangan Batas Kerangka Kerja : ".$penguranganBatasKK."<br/></p>";
      //
      // echo "<p>Batas Bawah Pengelolaan Aset : ".$batasBawahPA."<br/>";
      // echo "Batas Atas Pengelolaan Aset : ".$batasAtasPA."<br/>";
      // echo "Pengurangan Batas Pengelolaan Aset : ".$penguranganBatasPA."<br/></p>";
      //
      // echo "<p>Batas Bawah Teknologi : ".$batasBawahTeknologi."<br/>";
      // echo "Batas Atas Teknologi : ".$batasAtasTeknologi."<br/>";
      // echo "Pengurangan Batas Teknologi : ".$penguranganBatasTeknologi."<br/></p>";
      //
      // echo "<p>Batas Bawah Hasil : ".$batasBawahHasil."<br/>";
      // echo "Batas Atas Hasil : ".$batasAtasHasil."<br/>";
      // echo "Pengurangan Batas Hasil : ".$penguranganBatasHasil."<br/></p>";
      //
      // echo"<h4>Langkah 2 : Mencari Derajat Keanggotaan Setiap Data Training</h4>";
      // echo"<table border='1' align='center'><tr><td>";
      // echo"<b>Derajat Keanggotaan Tata Kelola</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataTataKelola);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahTK[$i]."</td><td>".$derajatKeanggotaanTinggiTK[$i]."</td></tr>";
      // }
      // echo"</table></td><td>";
      //
      // echo"<b>Derajat Keanggotaan Risiko</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataRisiko);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahRisiko[$i]."</td><td>".$derajatKeanggotaanTinggiRisiko[$i]."</td></tr>";
      // }
      // echo"</table></td><td>";
      //
      // echo"<b>Derajat Keanggotaan Kerangka Kerja</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataKerangkaKerja);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahKK[$i]."</td><td>".$derajatKeanggotaanTinggiKK[$i]."</td></tr>";
      // }
      // echo"</table></td><td>";
      //
      // echo"<b>Derajat Keanggotaan Pengelolaan Aset</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataPengelolaanAset);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahPA[$i]."</td><td>".$derajatKeanggotaanTinggiPA[$i]."</td></tr>";
      // }
      // echo"</table></td><td>";
      //
      // echo"<b>Derajat Keanggotaan Teknologi</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataTeknologi);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahTeknologi[$i]."</td><td>".$derajatKeanggotaanTinggiTeknologi[$i]."</td></tr>";
      // }
      // echo"</table></td><td>";
      //
      // echo"<b>Derajat Keanggotaan Hasil</b><br/>";
      // echo"<table border='1' align='center'><tr><th>No</th><th>Rendah</th><th>Tinggi</th></tr><tr>";
      // for($i=0;$i<count($dataHasil);$i++){
      //   echo"<td>".($i+1)."</td><td>".$derajatKeanggotaanRendahHasil[$i]."</td><td>".$derajatKeanggotaanTinggiHasil[$i]."</td></tr>";
      // }
      // echo"</table></td></tr></table>";
      //
      // echo"<h4>Langkah 3 : Mencari Rules</h4>";
      // echo"<table border='1' align='center'>";
      // for($i=0;$i<count($dataTataKelola);$i++){
      //   echo "<tr><td>Rules ".($i+1)."</td>";
      //   for($j=0;$j<count($rules[$i]);$j++){
      //     echo "<td>".$rules[$i][$j]."</td>";
      //   }
      //   echo "</tr>";
      // }
      // echo "</table>";
      //
      // echo"<h3>Langkah 2 : Implikasi</h3>";
      // echo"<table border='1' align='center'>";
      // for($i=0;$i<count($hasilImplikasi);$i++){
      //   echo "<tr><td>Rules ".($i+1)."</td>";
      //   for($j=0;$j<count($hasilImplikasi[$i]);$j++){
      //     echo "<td>".$hasilImplikasi[$i][$j]."</td>";
      //   }
      //   echo "</tr>";
      // }
      // echo "</table>";
      //
      // echo"<h3>Langkah 3 : Agregasi</h3>";
      // echo"<table border='1' align='center'>";
      // for($i=0;$i<count($agregasi);$i++){
      //   echo "<tr><td>Data ".($i+1)."</td>";
      //     echo "<td>".$agregasi[$i]."</td>";
      //   echo "</tr>";
      // }
      // echo "</table>";
      //
      //
      // echo"<h3>Langkah 4 : Defuzzyfikasi</h3>";
      // echo"<table border='1' align='center'>";
      // for($i=0;$i<count($defuzifikasi);$i++){
      //   echo "<tr><td>Data ".($i+1)."</td>";
      //     echo "<td>".$defuzifikasi[$i]."</td>";
      //   echo "</tr>";
      // }
      // echo "</table>";
      return $defuzifikasi[(count($defuzifikasi)-1)];
    }
}
