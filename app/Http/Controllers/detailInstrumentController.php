<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\crudDetailInstrument;
use App\crudInstrument;

class detailInstrumentController extends Controller
{
    public $halaman = 'kelolaVariable';

    public function tampilDetailInstrument($id){
          $instrument = crudInstrument::select('*')
                                        ->join('tb_variable','tb_variable.id_variable','tb_instrumen.id_variable')
                                        ->join('tb_judul_instrumen','tb_judul_instrumen.id_judul_instrumen','tb_instrumen.id_judul_instrumen')
                                        ->where('tb_instrumen.id_instrumen',$id)
                                        ->get();

          $instruments = crudDetailInstrument::select('*')
                                        ->join('tb_instrumen','tb_detail_instrumen.id_instrumen','tb_instrumen.id_instrumen')
                                        ->join('tb_parameter','tb_detail_instrumen.id_parameter','tb_parameter.id_parameter')
                                        ->where('tb_instrumen.id_instrumen',$id)
                                        ->paginate(10);
          return view('halamanInstrumen.tampil-detail-instrument',['instruments'=>$instruments, 'a'=>$instrument,'id'=>$id, 'halaman'=>$this->halaman]);
    }
    
}
