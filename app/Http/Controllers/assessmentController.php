<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\crudAsessment;
use App\crudJawabanInstrument;
use App\crudVariable;
use App\crudSaran;

class assessmentController extends Controller
{
  public $halaman = "assessment";

  public function listAsesmen(){
      // cek apakah role assessor atau bukan
      if(\Auth::user()->role != "user"){
        redirect()->to('/')->send();
      }

      // inisialisasi data  dari model
      $dataJawabanInstrument = crudJawabanInstrument::select('tb_jawaban_instrumen.*')
                                                    ->join('tb_user','tb_user.id_user','tb_jawaban_instrumen.id_user')
                                                    ->where('tb_user.id_user',\Auth::user()->id_user)
                                                    ->groupBy('tb_jawaban_instrumen.id_user')
                                                    ->groupBy('tb_jawaban_instrumen.id_jawaban_instrumen')
                                                    ->orderBy('tb_jawaban_instrumen.id_jawaban_instrumen','DESC')
                                                    ->get();
                                                    
      $dataSaran = crudJawabanInstrument::select('*')
                                        ->join('tb_saran','tb_saran.id_jawaban_instrumen','tb_jawaban_instrumen.id_jawaban_instrumen')
                                        ->where('tb_jawaban_instrumen.id_user',\Auth::user()->id_user)
                                        ->groupBy('tb_jawaban_instrumen.id_jawaban_instrumen')
                                        ->groupBy('tb_jawaban_instrumen.id_user')
                                        ->get();

      $dataVariableTerisi = crudJawabanInstrument::select('*')
                                                ->join(\DB::raw('(tb_detail_instrumen inner join (tb_instrumen inner join tb_variable on tb_instrumen.id_variable=tb_variable.id_variable) on tb_instrumen.id_instrumen=tb_detail_instrumen.id_instrumen)'),'tb_detail_instrumen.id_detail_instrumen','tb_jawaban_instrumen.id_detail_instrumen')
                                                ->where('tb_jawaban_instrumen.id_user',\Auth::user()->id_user)
                                                ->groupBy('tb_variable.id_variable')
                                                ->groupBy('tb_jawaban_instrumen.id_user')
                                                ->groupBy('tb_jawaban_instrumen.id_jawaban_instrumen')
                                                ->get();
      $dataVariable = crudVariable::all();
      return view('halamanAssessment/tampil-asesmen',['data' => $dataJawabanInstrument, 'dataVariableTerisi' => $dataVariableTerisi, 'dataSaran' => $dataSaran, 'data2' => $dataVariable, 'halaman' => $this->halaman]);
  }
  public function prosesIsiSaran(Request $request){
    $rules = array(
        'isi_saran'=>'required',
    );
    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails()){
        return back()
            ->withErrors($validator) // tampilkan error input pada halaman login
            ->withInput(); // tampilkan semua input dalam form
    }else{
        try{
            $input = new crudSaran;
            $input->isi_saran  = $request->get('isi_saran');
            $input->id_jawaban_instrumen  = $request->get('id_jawaban_instrumen');
            $input->save();

            return \Redirect('tampil-detail-hasil-assessment/'.$request->get('id_jawaban_instrumen'));
        }catch(\Exception $e){
            return \Redirect('tampil-detail-hasil-assessment/'.$request->get('id_jawaban_instrumen'))->withErrors(["gagal" => "Gagal Input Data kedalam Database.".$e->getMessage()]);
        }
    }
  }
}
