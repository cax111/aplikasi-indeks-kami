<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJawabanInstrumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jawaban_instrumen', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('id_jawaban_instrumen');
            $table->integer('skor');
            $table->integer('id_user');
            $table->integer('id_detail_instrumen');
            $table->timestamps();

            $table->index(['id_user', 'id_detail_instrumen']);

            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->foreign('id_detail_instrumen')->references('id_detail_instrumen')->on('tb_detail_instrumen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jawaban_instrumen');
    }
}
