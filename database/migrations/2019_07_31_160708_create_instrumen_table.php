<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_instrumen', function (Blueprint $table) {
            $table->integer('id_instrumen')->autoIncrement();
            $table->text('isi_instrumen');
            $table->integer('bobot_instrumen');
            $table->integer('id_variable');
            $table->integer('id_judul_instrumen');

            $table->index(['id_variable', 'id_judul_instrumen']);

            $table->foreign('id_variable')->references('id_variable')->on('tb_variable');
            $table->foreign('id_judul_instrumen')->references('id_judul_instrumen')->on('tb_judul_instrumen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_instrumen');
    }
}
