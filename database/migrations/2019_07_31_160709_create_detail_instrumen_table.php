<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailInstrumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_instrumen', function (Blueprint $table) {
            $table->integer('id_detail_instrumen')->autoIncrement();
            $table->integer('id_instrumen');
            $table->integer('id_parameter');

            $table->index(['id_instrumen', 'id_parameter']);

            $table->foreign('id_instrumen')->references('id_instrumen')->on('tb_instrumen');
            $table->foreign('id_parameter')->references('id_parameter')->on('tb_parameter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_instrumen');
    }
}
