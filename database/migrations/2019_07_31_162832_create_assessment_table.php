<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_assessment', function (Blueprint $table) {
            $table->integer('id_assessment')->autoIncrement();
            $table->integer('id_instrumen');
            $table->integer('id_user');
            $table->integer('jawaban_parameter');

            $table->index(['id_instrumen', 'id_user']);

            $table->foreign('id_instrumen')->references('id_instrumen')->on('tb_instrumen');
            $table->foreign('id_user')->references('id_user')->on('tb_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_assessment');
    }
}
