<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndentitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_responden', function (Blueprint $table) {
            $table->integer('id_responden')->autoIncrement();
            $table->string('satuan_kerja', 50);
            $table->string('direktorat', 50);
            $table->string('departemen', 50);
            $table->string('alamat1', 200);
            $table->string('alamat2', 200);
            $table->string('kota', 100);
            $table->integer('kode_pos');
            $table->string('nomor_telepon', 15);
            $table->string('email', 50);
            $table->string('pengisi_responden', 50);
            $table->string('nip_responden', 20);
            $table->string('jabatan_responden', 50);
            $table->dateTime('tanggal_pengisian');
            $table->text('deskripsi_ruang_lingkup');
            $table->integer('id_user');
            $table->timestamps();

            
            $table->index(['id_user']);

            $table->foreign('id_user')->references('id_user')->on('tb_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_responden');
    }
}
