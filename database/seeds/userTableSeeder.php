<?php

use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_user')->insert([
            'username' => 'admin123',
            'password' => bcrypt('12345678'),
            'role' => 'admin',
            'remember_token' => '',
        ]);
    }
}
